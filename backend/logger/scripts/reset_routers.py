from logger.models import ApparentSensor, Router, Sensor


def run():
    Router.objects.all().delete()
    for a_s in ApparentSensor.objects.all():
        try:
            s = Sensor.objects.get(name=a_s.name)
        except Sensor.DoesNotExist:
            continue
        r = Router(apparent_sensor=a_s, aggregate="max")
        r.save()
        r.sensors.add(s)

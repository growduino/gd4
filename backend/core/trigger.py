from datetime import datetime
import logging
from typing import Any

from django.db import connection
from django.http import JsonResponse

# from ..fanconfig import FanConfig
from core.models import LcdLine, OutputRec, Trigger
from logger.mqtt import mqtt_client

log = logging.getLogger(__name__)


def quiet_send(msg: str) -> None:
    try:
        mqtt_client.publish("logger/internal", msg)
    except Exception as e:
        log.warning(f"Sending MQTT message failed: {msg}")
        print(e)  # noqa: T201  # print


# def check_power_panic():
#     power = [
#         p.value == 3
#         for p in Sensor.objects.get(
#             name="PowerState", logger__name="internal"
#         ).data.order_by("-time")[:3]
#     ]
#     battery = [
#         b.value < 6
#         for b in Sensor.objects.get(
#             name="Battery", logger__name="internal"
#         ).data.order_by("-time")[:3]
#     ]
#
#     if all(power) and all(battery) and len(power) > 2:
#         # last three measures are no power avaliable and battery under 6%
#
#         quiet_send("set lcd_clear")
#         messages = [
#             "Power Shutdown",
#             "Battery Depleted",
#             "Turn big switch OFF",
#             "Push UPS-OFF button",
#         ]
#         for msg in messages:
#             quiet_send(f"set lcd {msg}")
#         raise Exception("Power panic")


def tick_triggers() -> None:
    for trigger in Trigger.objects.filter(active=1):
        trigger.tick()
        alerts = trigger.alert.all()
        for alert in alerts:
            alert.process_alert()

    # fan_power = FanConfig(get_config(filename="fanconfig"), get_config(filename="client")).tick()
    # ecfan_sensor, created = Sensor.objects.get_or_create(
    #     name="ECFan", defaults={"name": "ECFan", "index": 12, "divisor": 1}
    # )
    # if created:
    #     ecfan_sensor.save()
    # ecfan_sensor.log(fan_power)
    # fan_power = int(fan_power * 2.55)
    # quiet_send(f"set ec_fan {fan_power}")

    for output in OutputRec.objects.all():
        output.tick()


def send_switch() -> None:
    """
    Send switch command to logger daemon, so he can change outputs as soon as possible.
    """
    quiet_send("switch")


def send_lcd_lines() -> None:
    quiet_send("set lcd_clear")

    for lcd_line in LcdLine.objects.all():
        quiet_send("set lcd " + lcd_line.display())


def run_cron(*_args: Any, **_kwargs: Any) -> JsonResponse:
    log.debug("Running cron")
    tick_triggers()
    send_switch()
    send_lcd_lines()
    return JsonResponse(
        {"result": "ok"},
    )


def generate_averages(*_args: Any, **_kwargs: Any) -> JsonResponse:
    start = datetime.now()
    with connection.cursor() as cursor:
        cursor.execute(
            "with h as (select max(time) as time from logger_sensordatahourly) delete from logger_sensordatahourly where time = (select time from h)"
        )
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from logger_sensordatahourly) insert into logger_sensordatahourly (value, sensor_id, time) select avg(value) value,sensor_id,date_trunc('hour', time) as time from logger_sensordata where date_trunc('hour', time) > (select time from h) group by sensor_id,date_trunc('hour', time)"
        )
        cursor.execute(
            "with h as (select max(time) as time from logger_sensordatadaily) delete from logger_sensordatadaily where time = (select time from h)"
        )
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from logger_sensordatadaily) insert into logger_sensordatadaily (value, sensor_id, time) select avg(value) value,sensor_id,date_trunc('day', time) as time from logger_sensordata where date_trunc('day', time) > (select time from h) group by sensor_id,date_trunc('day', time)"
        )
        cursor.execute(
            "with h as (select max(time) as time from logger_apparentsensordatahourly) delete from logger_apparentsensordatahourly where time = (select time from h)"
        )
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from logger_apparentsensordatahourly) insert into logger_apparentsensordatahourly (value, sensor_id, time) select avg(value) value,sensor_id,date_trunc('hour', time) as time from logger_apparentsensordata where date_trunc('hour', time) > (select time from h) group by sensor_id,date_trunc('hour', time)"
        )
        cursor.execute(
            "with h as (select max(time) as time from logger_apparentsensordatadaily) delete from logger_apparentsensordatadaily where time = (select time from h)"
        )
        cursor.execute(
            "with h as (select coalesce(max(time),'2018-01-01 00:00:00+00') as time from logger_apparentsensordatadaily) insert into logger_apparentsensordatadaily (value, sensor_id, time) select avg(value) value,sensor_id,date_trunc('day', time) as time from logger_apparentsensordata where date_trunc('day', time) > (select time from h) group by sensor_id,date_trunc('day', time)"
        )

        end = datetime.now()

    return JsonResponse(
        {"result": "ok", "duration": (end - start).total_seconds()},
    )

#!/bin/sh

cd $(dirname $0)/..

echo "Running ruff format+check ..."
bin/ruff.sh

echo "Running mypy ..."
bin/mypy.sh

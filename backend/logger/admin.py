from django.contrib import admin

from .models import (
    ApparentSensor,
    ApparentSensorData,
    Firmware,
    Logger,
    Router,
    Sensor,
    SensorData,
)


@admin.register(Logger)
class LoggerAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    search_fields = ("name",)


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "sensor_type",
        "logger",
        "shift",
        "slope",
        "lower_limit",
        "upper_limit",
    )
    list_filter = ("logger",)
    search_fields = ("name",)


@admin.register(SensorData)
class SensorDataAdmin(admin.ModelAdmin):
    list_display = ("id", "time", "value", "sensor")
    list_filter = ("time", "sensor")


@admin.register(ApparentSensor)
class ApparentSensorAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "index", "sensor_type")
    search_fields = ("name",)


@admin.register(ApparentSensorData)
class ApparentSensorDataAdmin(admin.ModelAdmin):
    list_display = ("id", "time", "value", "sensor")
    list_filter = ("time", "sensor")


@admin.register(Router)
class RouterAdmin(admin.ModelAdmin):
    list_display = ("id", "apparent_sensor", "aggregate")
    list_filter = ("apparent_sensor",)
    # raw_id_fields = ("sensors",)


@admin.register(Firmware)
class FirmwareAdmin(admin.ModelAdmin):
    actions = ["check_for_updates"]
    list_display = ("name",)

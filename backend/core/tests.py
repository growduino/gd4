from django.test import TestCase

from logger.models import Logger, Sensor

# Create your tests here.
from .models import OutputRec

LOGGED_VAL_1 = 10
LOGGED_VAL_2 = 20


class SensorTestCase(TestCase):
    def setUp(self):
        self.logger = Logger(name="test logger")
        self.logger.save()

        Sensor.objects.create(name="test1", index=1)
        Sensor.objects.create(name="test2", index=2)

    def test_sensor_create_and_log(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.log(LOGGED_VAL_1)
        test2.log(LOGGED_VAL_2)
        assert test1.data.latest().value == LOGGED_VAL_1
        assert test2.data.latest().value == LOGGED_VAL_2

    def test_calib_offset(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.offset = LOGGED_VAL_1
        test2.offset = LOGGED_VAL_2

        test1.save()
        test2.save()

        assert test1.log(10) == LOGGED_VAL_1
        assert test2.log(20) == LOGGED_VAL_2

    def test_calib_slope(self):
        test1 = Sensor.objects.get(index=1)
        test2 = Sensor.objects.get(index=2)

        test1.slope = 2
        test2.slope = 4

        test1.save()
        test2.save()

        assert test1.log(10) == 20
        assert test2.log(20) == 80

    def test_log_latest(self):
        test1 = Sensor.objects.get(index=1)
        assert test1.log(10) == 10

        assert test1.log(11) == 11
        assert test1.log(12) == 12
        assert test1.log(13) == 13

        assert test1.data.latest().value == 13


class OutputTestCase(TestCase):
    def setUp(self):
        OutputRec.objects.create(index=0, name="test0")

    def test_created(self):
        output = OutputRec.objects.get(index=0)
        assert output.name == "test0"
        assert not output.disabled
        assert not output.state

    def test_toggle(self):
        output = OutputRec.objects.get(index=0)
        assert output.switch(True)
        assert output.state

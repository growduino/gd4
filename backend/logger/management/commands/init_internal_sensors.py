from typing import Any

from django.core.management.base import BaseCommand

from logger.models import Logger
from logger.utils import create_sensors


class Command(BaseCommand):
    help = "Create default internal logger, if its missing. Use --reset to delete it and create new."
    requires_migrations_checks = True

    def handle(self, *_args: Any, **options: Any) -> None:
        if "reset" in options and options["reset"]:
            print("Deleting data")  # noqa: T201
            Logger.objects.filter(name="internal").delete()

        if Logger.objects.filter(id=1).count() == 0:
            print("Creating internal logger")  # noqa: T201
            logger = Logger(id=1, name="internal", mac="00:00:00:00:00:01")
            logger.save()
        for logger in Logger.objects.all():
            if not logger.sensors.exists():
                create_sensors(logger)

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete internal logger and create new, including its sensors. This erases history logs.",
        )

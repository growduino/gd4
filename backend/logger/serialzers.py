from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from core.models import LcdLine
from logger.models import ApparentSensor, Firmware, Logger, Router, Sensor
from logger.mqtt import mqtt_client


class ApparentSensorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ApparentSensor
        fields = ["id", "index", "name", "sensor_type"]


class SensorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sensor
        fields = [
            "id",
            "name",
            "sensor_type",
            "logger",
            "shift",
            "slope",
            "lower_limit",
            "upper_limit",
        ]


class FirmwareSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Firmware
        fields = ["id", "name"]


class LcdLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = LcdLine
        fields = [
            "id",
            "priority",
            "text",
            "error_text",
            "error_age",
            "sensor",
            "apparent_sensor",
            "is_datetime",
        ]


class LoggerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Logger
        fields = ["id", "name", "mac", "config", "sensors", "firmware"]

    sensors = HyperlinkedIdentityField(view_name="logger-sensor-list", lookup_url_kwarg="logger_pk")

    def create(self, validated_data):
        new_name = validated_data.get("config").get("board_name")
        validated_data["name"] = new_name
        return super().create(validated_data)

    def update(self, instance, validated_data):
        new_name = validated_data.get("config").get("board_name")
        if new_name:
            validated_data["name"] = new_name
        mqtt_client.publish(f"logger/{instance.id}", "cfg")
        return super().update(instance, validated_data)


class RouterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Router
        fields = ["id", "name", "apparent_sensor", "aggregate", "sensors"]

    sensors = NestedHyperlinkedRelatedField(
        many=True,
        queryset=Sensor.objects.all(),
        view_name="logger-sensor-detail",
        parent_lookup_kwargs={"logger_pk": "logger__pk"},
        # ^-- Nameserver queryset will .filter(domain__pk=domain_pk)
        #     being domain_pk (ONE underscore) value from URL kwargs
    )

#!/bin/sh

set -x

if [ -z "$DJANGO_ADMIN_USERNAME" ]; then
  DJANGO_ADMIN_USERNAME="grdw"
fi
if [ -z "$DJANGO_ADMIN_EMAIL" ]; then
  DJANGO_ADMIN_EMAIL="djangoadmin@growduino.cz"
fi

if [ "$DJANGO_RESET_DB" ]; then
  DJANGO_RESET_DB="--reset"
fi

poetry run ./manage.py migrate
poetry run ./manage.py createsuperuser --noinput --username $DJANGO_ADMIN_USERNAME --email $DJANGO_ADMIN_EMAIL
poetry run ./manage.py init_grdw $DJANGO_RESET_DB
#if [ -nz "$LOGGER_PASS" ]; then
#	poetry run ./manage.py runscript 
#fi
poetry run ./manage.py collectstatic --no-input

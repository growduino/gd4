import logging
from typing import TYPE_CHECKING, Any, TypedDict, cast

from rest_framework import response, serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework_nested import routers

from core.models import LcdLine
from logger.models import ApparentSensor, Firmware, Logger, Router, Sensor
from logger.mqtt import mqtt_client
from logger.serialzers import (
    ApparentSensorSerializer,
    FirmwareSerializer,
    LcdLineSerializer,
    LoggerSerializer,
    RouterSerializer,
    SensorSerializer,
)
from logger.utils import create_sensors

log = logging.getLogger(__name__)

if TYPE_CHECKING:
    RouterModelViewSet = viewsets.ModelViewSet[Router]
    LcdLineModelViewSet = viewsets.ModelViewSet[LcdLine]
    FirmwareModelViewSet = viewsets.ModelViewSet[Firmware]
    ApparentSensorModelViewSet = viewsets.ModelViewSet[ApparentSensor]
    SensorModelViewSet = viewsets.ModelViewSet[Sensor]
    LoggerModelViewSet = viewsets.ModelViewSet[Logger]
else:
    RouterModelViewSet = viewsets.ModelViewSet
    LcdLineModelViewSet = viewsets.ModelViewSet
    FirmwareModelViewSet = viewsets.ModelViewSet
    ApparentSensorModelViewSet = viewsets.ModelViewSet
    SensorModelViewSet = viewsets.ModelViewSet
    LoggerModelViewSet = viewsets.ModelViewSet


class RouterViewSet(RouterModelViewSet):
    queryset = Router.objects.all()
    serializer_class = RouterSerializer


class ApparentSensorViewSet(ApparentSensorModelViewSet):
    queryset = ApparentSensor.objects.all()
    serializer_class = ApparentSensorSerializer


class SensorViewSet(SensorModelViewSet):
    def get_queryset(self):
        if "logger_pk" in self.kwargs:
            return Sensor.objects.filter(logger=self.kwargs["logger_pk"])
        return Sensor.objects.all()

    serializer_class = SensorSerializer

    def create(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        return super().create(request, *args, **kwargs)


class LogRecord(TypedDict):
    name: str
    value: float
    age: int
    idx: int


class LoggerViewSet(LoggerModelViewSet):
    queryset = Logger.objects.all()
    serializer_class = LoggerSerializer

    @action(detail=True, methods=["post"])
    def log(self, request: Request, *_args: Any, **_kwargs: Any) -> Response:
        logger = self.get_object()
        for record in request.data:
            record = cast(LogRecord, record)
            log.debug("Record: %s", record)
            logger.sensors.get(name=record["name"]).log(record["value"])
            # Sensor.objects.get(name=record["name"]).log(record["value"], record["age"], record["idx"])

        return response.Response({"result": 201, "message": "logged"}, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=["get"])
    def ages(self, *_args: Any, **_kwargs: Any) -> Response:
        loggers = Logger.objects.all()
        ages = [logger.as_age_info() for logger in loggers]
        return response.Response(ages, status=status.HTTP_200_OK)

    @action(detail=True, methods=["post"])
    def flash(
        self,
        request: Request,
        firmware_version: str | None = None,
        *_args: Any,
        **_kwargs: Any,
    ) -> Response:
        if firmware_version is None:
            Firmware.refresh_from_api()
            firmware_version = Firmware.objects.latest().name
        elif not Firmware.objects.filter(name=firmware_version).exists():
            return response.Response(
                {"result": 404, "message": f"Firmware {firmware_version} not found."},
                status=status.HTTP_404_NOT_FOUND,
            )

        return response.Response(
            {"result": 501, "message": "Well yes but no."},
            status=status.HTTP_501_NOT_IMPLEMENTED,
        )

    @action(detail=True, methods=["get"])
    def flash_status(self, request: Request, pk: int) -> Response:
        return response.Response(
            {"result": 501, "message": "Definitely not flashing."},
            status=status.HTTP_501_NOT_IMPLEMENTED,
        )

    @action(detail=True, methods=["post"])
    def reboot(self, request: Request, pk: int) -> Response:
        logger = self.get_object()
        mqtt_client.publish(f"logger/{logger.id}", "rst")
        return response.Response(
            {"result": 200, "message": f"Reset message sent to {logger.name}."},
            status=status.HTTP_200_OK,
        )

    def create(self, request: Request) -> Response:
        log.warning("Create logger: %s", request.data)
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except serializers.ValidationError as e:
            if "mac" in e.detail and e.detail["mac"][0].code == "unique":  # type:ignore
                log.warning(">>> %s", e.detail["mac"])  # type:ignore[call-overload]
                obj = Logger.objects.get(mac=request.data["mac"])
                serializer = self.get_serializer(obj)
                return response.Response(serializer.data, status=status.HTTP_200_OK)
            return self.handle_exception(e)
        self.perform_create(serializer)
        assert serializer.instance is not None
        create_sensors(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        log.warning("Update logger: %s", request.data)
        return super().update(request, *args, **kwargs)

    def partial_update(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        log.warning("Partial update logger: %s", request.data)
        return super().partial_update(request, *args, **kwargs)


class FirmwareViewSet(FirmwareModelViewSet):
    queryset = Firmware.objects.all()
    serializer_class = FirmwareSerializer

    @action(detail=False, methods=["get"])
    def refresh(self, *_args: Any, **_kwargs: Any) -> Response:
        return response.Response(
            {"result": Firmware.refresh_from_api()},
            status=status.HTTP_200_OK,
        )


class LcdLineViewSet(LcdLineModelViewSet):
    queryset = LcdLine.objects.all()
    serializer_class = LcdLineSerializer

    @action(detail=False, methods=["get"])
    def render(self, request: Request) -> Response:
        qs = self.get_queryset()
        return response.Response(
            {"lines": [line.display() for line in qs]},
            status=status.HTTP_200_OK,
        )


router = routers.DefaultRouter()
router.register(r"router", RouterViewSet)
router.register(r"firmware", FirmwareViewSet)
router.register(r"lcd_line", LcdLineViewSet)
router.register(r"apparent_sensor", ApparentSensorViewSet)
router.register(r"sensor", SensorViewSet, basename="sensor")
# router.register(r"logger/(?P<logger_id>[a-z0-9_]+)/sensor", SensorViewSet)
router.register(r"logger", LoggerViewSet)
logger_router = routers.NestedSimpleRouter(router, r"logger", lookup="logger")
logger_router.register(r"sensor", SensorViewSet, basename="logger-sensor")

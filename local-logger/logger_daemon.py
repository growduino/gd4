import argparse
import datetime
import fcntl
import json
import os
import socket
import struct
import time
from logging.handlers import TimedRotatingFileHandler
import logging
from typing import Any

import requests
import serial

import paho.mqtt.client as mqtt
from paho.mqtt.client import ConnectFlags  # noqa
from paho.mqtt.reasoncodes import ReasonCode  # noqa

logger = logging.getLogger(__name__)

logger.setLevel("INFO")
formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(lineno)s: %(message)s")
handler = TimedRotatingFileHandler(
    "/tmp/loggerdaemon.log", when="midnight", backupCount=3
)
handler.setFormatter(formatter)
logger.addHandler(handler)


def get_ip_address(ifname: bytes):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(
            fcntl.ioctl(s.fileno(), 0x8915, struct.pack(b"256s", ifname))[20:24]
        )  # SIOCGIFADDR
    except OSError:
        return None


# json decode error is value error in older pythons
try:
    json_decode_error = json.decoder.JSONDecodeError
except AttributeError:
    json_decode_error = ValueError


class Connection:
    def __init__(self, port, speed=115200, timeout=3):
        logger.debug("Starting up")
        self.port = port
        self.speed = speed
        self.connection = serial.Serial(port, speed, timeout=timeout)
        self.offset = 10  # Which second in minute we want to get data. Sensor reading starts at :00
        self.output_url = self.create_url(
            "output_states.jso"
        )  # url to get wanted output states from
        self.log_url = self.create_url("log")  # url to write sensor data to
        self.calib_url = self.create_url("calib.jso")  # url to get calibration data
        self.age = -1

        logger.debug("Connecting to arduino")
        try:
            self.read()  # get rid of Hello #name
        except UnicodeDecodeError:
            pass

        logger.debug("Inititalising MQ")
        self.mqtt_client = mqtt.Client(
            client_id="local-logger",
            callback_api_version=mqtt.CallbackAPIVersion.VERSION2,  # noqa
            clean_session=True,
        )
        self.mqtt_client.on_connect = self.on_mqtt_connect
        self.mqtt_client.on_message = self.on_mqtt_message
        self.mqtt_client.connect("localhost", 1883, 60)
        self.mqtt_client.loop_start()

        self.data_line = None

        logger.debug("Loading calibration")
        self.load_calibration()

    @staticmethod
    def on_mqtt_connect(
        client: mqtt.Client,
        userdata: Any,
        flags: ConnectFlags,
        reason_code: ReasonCode,
        properties=None,
    ) -> None:
        print(f"Connected with result code {reason_code}")
        client.subscribe("logger/internal")

    def on_mqtt_message(
        self,
        client: mqtt.Client,
        userdata: Any,
        msg: mqtt.MQTTMessage,
    ) -> None:
        print(msg.topic + " " + str(msg.payload))
        self.process_mq_data(msg.payload)

    def create_url(self, text):
        """Format url for data upload
        :param text:path in url (after host name)
        :return text:full url
        """
        url = "http://localhost"
        return f"{url}/{text}"

    def read(self):
        """read line from serial port and return it as python string"""
        data = self.connection.readline()
        data = data.decode("utf-8", errors="ignore")
        logger.debug("<<< " + data)
        return data

    def read_data(self, reads=10, ok_is_ok=True):
        """get line from serial, parse and return dict, or None after too many failures/after getting ok
        Sort-of throws away debug messages"""

        while reads > 0:
            json_data = None
            self.data_line = None
            data = self.read().strip()

            if not data:
                continue

            if data.lower() == "not recognized":
                return None

            if data.lower() == "ok":
                if ok_is_ok:
                    self.data_line = data
                    return None
                else:
                    continue
            if data[0] not in ["{", "["]:
                continue
            try:
                json_data = json.loads(data)
            except json_decode_error:
                reads -= 1
                time.sleep(0.1)
            self.data_line = data
            return json_data

    def write_(self, data):
        """Convert data to bytestream and send to arduino as line"""
        if isinstance(data, bytes):
            data = data.decode("utf-8")
        logger.debug(">>> " + data)
        data = data.strip() + "\n"
        data = data.encode("utf-8").replace(
            b"\xc2\xb0", b"\xdf"
        )  # recode degree symbol to HD44780 charset
        self.connection.write(data)
        time.sleep(0.1)

    def robust_write(self, data, writes=3, reads=30, ok_is_ok=True):
        """write data to arduino and wait for response, repeating as needed"""
        self.data_line = None
        write_counter = 0
        json_data = None
        while self.data_line is None and write_counter < writes:
            write_counter += 1
            self.write_(data)
            json_data = self.read_data(reads=reads, ok_is_ok=ok_is_ok)

        if self.data_line is None:
            logger.debug(f"Robust read failed: {data}")
        else:
            logger.debug(f"Got data: {self.data_line}")
        return json_data

    def read_sensors(self):
        """get sensor data from arduino and convert"""
        return self.robust_write("get sensors", ok_is_ok=False)

    def log(self, data):
        """Log data to django"""
        if data:
            body = json.dumps(data)
            logger.debug("data inside logging: %s", data)
            result = self.mqtt_client.publish("log/internal", body)
            time.sleep(0.1)
            try:
                logger.debug(
                    "publish result: %s, published? %s", result, result.is_published()
                )
            except (ValueError, RuntimeError) as e:
                logger.error(e)

    def switch(self, depth=0):
        """Get wanted output states from django and send them to arduino"""
        if depth > 10:
            logger.error("grdw logger daemon: switch() failed")
            return False
        try:
            r = requests.get(self.output_url)
            result = json.loads(r.text)
            logger.debug(result)
            self.set_outputs(result["outputs"])
            if "raw" in result:
                self.get_raw(result["raw"])

        except ValueError as e:
            print(e)
            self.switch(depth + 1)

    def process_mq_data(self, payload=None) -> None:
        if payload is None:
            return
        if isinstance(payload, bytes):
            payload = payload.decode("utf-8")

        logger.debug("Processing mq data:" + payload)
        if payload == "calib":
            self.load_calibration()
        elif payload == "switch":
            self.switch()
        elif " " in payload:
            data = self.robust_write(payload)
            self.log(data)
            logger.debug(data)

    def poll(self):
        """Main worker procedure, contains daemon loop"""
        time.sleep(1)
        self.switch()
        while True:
            now = datetime.datetime.now()
            self.robust_write(f"set offset {now.second}")
            logger.debug(now)

            sleep_time = self.offset - now.second
            if sleep_time < 0:
                sleep_time = sleep_time + 60
            logger.debug(f"Sleeping for {sleep_time}s")
            time.sleep(sleep_time)
            cntr = 0
            self.data_line = None
            data = None
            while self.data_line is None and cntr < 3:
                cntr += 1
                data = self.read_sensors()
            if data and self.data_line is not None:
                try:
                    self.age = data[0]["age"]
                    self.log(data)
                except TypeError:
                    logger.error(
                        f"grdw logger daemon: poll: Data read error: {type(data)} {data}"
                    )
            else:
                logger.error("grdw logger daemon: poll: Data read error")
            time.sleep(15)
            self.switch()

    def set_outputs(self, param):
        """Send wanted output state to arduino"""
        self.robust_write(f"set outputs {param}")

    def get_raw(self, name):
        logger.debug("Getting raw value")
        self.robust_write(f"get raw {name}")
        data = json.loads(self.data_line)
        self.log(data)

    def load_calibration(self):
        try:
            r = requests.get(self.calib_url)
        except requests.exceptions.ConnectionError:
            logger.error("Cannot load calib data (Conn)")
            return
        try:
            calib_data = json.loads(r.text)
        except json.decoder.JSONDecodeError:
            logger.error("Cannot load calib data")
            calib_data = {}
        if "calibration_data" in calib_data:
            logger.debug("Removing server-side calibration from calib_data")
            calib_data.pop("calibration_data")
        for key in calib_data:
            val = calib_data[key]
            self.robust_write(f"set {key} {val}")


def report_lcd(connection):
    connection.robust_write("set lcd_clear")
    connection.robust_write("set lcd Connected")
    ipaddr = get_ip_address(b"eth0")
    if ipaddr:
        connection.robust_write(f"set lcd @{ipaddr}")
    ipaddr = get_ip_address(b"wlan0")
    if ipaddr:
        connection.robust_write(f"set lcd @{ipaddr}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Growduino reader daemon")
    parser.add_argument("-p", "--port", help="usb port name")
    parser.add_argument("-l", "--log", help="Loglevel (DEBUG, WARNING)")

    logger.info(f"Logger starting at {datetime.datetime.now().isoformat()}")

    args = parser.parse_args()
    if not args.port:
        for port in ("USB0", "ACM0", "USB1", "ACM1"):
            port_path = f"/dev/tty{port}"
            if os.path.exists(port_path):
                args.port = port_path
                break

    if not args.port or not os.path.exists(args.port):
        raise ValueError("Usb port not found")

    if not args.log:
        args.log = "warning"

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % args.log)

    logger.setLevel(numeric_level)

    handler2 = logging.StreamHandler()
    handler2.setLevel(args.log.upper())
    handler2.setFormatter(formatter)
    logger.addHandler(handler2)

    logger.info("Starting")
    connection = Connection(args.port)
    time.sleep(10)
    report_lcd(connection)
    connection.poll()

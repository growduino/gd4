from rest_framework import viewsets

from logger.models import Logger


class LoggerViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Logger.objects.filter(domain=self.kwargs["logger_pk"])

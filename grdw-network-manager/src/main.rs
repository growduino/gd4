mod models;

use axum::{Json, Router, routing::get};
use axum::extract::Path;
use network_manager::{DeviceType, NetworkManager};
use crate::models::{Wifi, NetInterface};


async fn get_wifis() -> Json<Vec<Wifi>> {
    let manager = NetworkManager::new();
    let devices = manager.get_devices().unwrap_or_default();
    let mut access_points = devices
        .into_iter()
        .filter(|device| device.device_type() == &DeviceType::WiFi)
        .flat_map(|device| device.as_wifi_device().unwrap().get_access_points().unwrap())
        .map(|ap| Wifi {
            ssid: ap.ssid.as_str().unwrap_or("").to_string(),
            strength: ap.strength,
        })
        .filter(|ap| !ap.ssid.is_empty())
        .collect::<Vec<Wifi>>();


    access_points.sort();

    let mut ssids: Vec<String> = Vec::new();

    let filtered_ap = access_points.into_iter().filter(|ap| {
        if ssids.contains(&ap.ssid) {
            false
        } else {
            ssids.push(ap.ssid.clone());
            true
        }
    }).collect::<Vec<Wifi>>();

    Json(filtered_ap)
}

async fn get_interfaces() -> Json<Vec<NetInterface>> {
    let manager = NetworkManager::new();
    let devices = manager.get_devices().unwrap_or_default();
    let interfaces = devices
        .into_iter()
        .filter(|device| device.device_type() == &DeviceType::WiFi || device.device_type() == &DeviceType::Ethernet)
        .map(|device| NetInterface::from_device(&device))
        .collect::<Vec<NetInterface>>();
    Json(interfaces)
}

async fn get_interface_by_name(Path(name): Path<String>) -> Json<NetInterface> {
    let manager = NetworkManager::new();
    let devices = manager.get_devices().unwrap_or_default();
    let interface = devices
        .into_iter()
        .filter(|device| device.interface() == name)
        .map(|device| NetInterface::from_device(&device))
        .collect::<Vec<NetInterface>>();
    if interface.is_empty() {
        return Json(NetInterface {
            name: String::new(),
            is_active: false
        });
    }
    Json(interface[0].clone())
}

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", get(|| async { "Hello, World!" }))
        .route("/wifis", get(get_wifis))
        .route("/interfaces", get(get_interfaces))
        .route("/interfaces/:name", get(get_interface_by_name));

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
#!/bin/sh

set -e -x

export GRDW_TIMEZONE=`poetry run python3 /opt/app/bin/extract-tz.py`

gunicorn --name=gunicorn_gd4 \
  --bind=0.0.0.0:8000 \
  --worker-tmp-dir=/dev/shm \
  --workers=1 \
  --threads=2 \
  --worker-connections=50 \
  --max-requests=100 \
  --timeout=60 \
  --worker-class=gthread \
  --log-file=- \
  --log-level=info \
  --pythonpath=/opt/app \
  gd4.wsgi:application \

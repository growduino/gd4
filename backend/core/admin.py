from django.contrib import admin

from logger.models import CalibrationData

from .models import (
    Alert,
    AlertLog,
    EventTimes,
    JsonStorage,
    LcdLine,
    OutputLog,
    OutputRec,
    Trigger,
    TriggerLog,
)


@admin.register(JsonStorage)
class JsonStorageAdmin(admin.ModelAdmin):
    list_display = ("id", "value", "filename", "time")
    list_filter = ("time",)


@admin.register(OutputRec)
class OutputRecAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "index",
        "target_index",
        "disabled",
    )
    list_filter = ("disabled",)
    search_fields = ("name",)


@admin.register(CalibrationData)
class CalibrationDataAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "sensor_reading",
        "real_value",
        "timestamp",
        "sensor",
    )
    list_filter = ("timestamp", "sensor")


@admin.register(Trigger)
class TriggerAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "index",
        "t_since",
        "t_until",
        "on_value",
        "off_value",
        "sensor",
        "output",
        "active",
    )


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "index",
        "on_message",
        "off_message",
        "trigger",
        "target",
        "active",
    )
    list_filter = ("trigger",)


@admin.register(TriggerLog)
class TriggerLogAdmin(admin.ModelAdmin):
    list_display = ("id", "trigger", "state", "time")
    list_filter = ("state", "time")
    raw_id_fields = ("trigger",)


@admin.register(OutputLog)
class OutputLogAdmin(admin.ModelAdmin):
    list_display = ("id", "output", "state", "time")
    list_filter = ("state", "time")
    raw_id_fields = ("output",)


@admin.register(AlertLog)
class AlertLogAdmin(admin.ModelAdmin):
    list_display = ("id", "alert", "state", "time")
    list_filter = ("alert", "state", "time")


@admin.register(EventTimes)
class EventTimesAdmin(admin.ModelAdmin):
    list_display = ("id", "event", "time")
    list_filter = ("time",)


@admin.register(LcdLine)
class LcdLineAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "priority",
        "text",
        "error_text",
        "error_age",
        "sensor",
        "apparent_sensor",
        "is_datetime",
    )
    list_filter = ("sensor", "is_datetime")

INTERNAL_SENSORS = (
    ("Humidity", "humidity", 0, 100, 0),
    ("no sensor", "Other", None, None, None),
    ("Temp1", "temp", -50, 120, 1),
    ("Light1", "light", 0, 32000, 2),
    ("Usnd", "dist", 4, 200, 3),
    ("Temp2", "temp", -50, 120, 4),
    ("Light2", "light", 0, 32000, 5),
    ("Temp3", "temp", -50, 120, 6),
    ("EC", "ec", 0, 10, 7),
    ("pH", "ph", 0, 14, 8),
    ("CO2", "CO2", 0, 5000, 9),
    ("Battery", "Other", 0, 100, 10),
    ("PowerState", "Other", None, None, 11),
    ("EC Fan", "Other", 0, 100, 12),
    ("PAR1", "light", 0, 3000, 13),
    ("PAR2", "light", 0, 3000, 14),
)

APPARENT_SENSORS = (
    ("Humidity", "humidity", "0"),
    ("no sensor", "Other", "-1"),
    ("Temp1", "temp", "1"),
    ("Light1", "light", "2"),
    ("Usnd", "dist", "3"),
    ("Temp2", "temp", "4"),
    ("Light2", "light", "5"),
    ("Temp3", "temp", "6"),
    ("EC", "ec", "7"),
    ("pH", "ph", "8"),
    ("CO2", "CO2", "9"),
    ("Battery", "Other", "10"),
    ("PowerState", "Other", "11"),
    ("EC Fan", "Other", "12"),
    ("PAR1", "light", "13"),
    ("PAR2", "light", "14"),
)

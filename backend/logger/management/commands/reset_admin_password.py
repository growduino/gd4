import os
from typing import Any

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Reset the admin password to default"

    def handle(self, *_args: Any, **_options: Any) -> None:
        from django.contrib.auth.models import User

        username = os.environ.get("DJANGO_ADMIN_USERNAME", "grdw")
        password = os.environ.get("DJANGO_ADMIN_PASSWORD", None)

        if password:
            try:
                user = User.objects.get(username=username)
                user.set_password(password)
                user.save()
                print(f"Password reset for user '{username}'")  # noqa: T201
            except User.DoesNotExist:
                print(f"User '{username}' not found")  # noqa: T201
            except Exception as e:
                print(f"Error: {e}")  # noqa: T201

from datetime import datetime
from decimal import Decimal
import logging
import math
from statistics import mean
from typing import TYPE_CHECKING, TypedDict, cast

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import QuerySet
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

log = logging.getLogger(__name__)


class LoggerAgeInfo(TypedDict):
    idx: int
    name: str
    age: int | None


def crop_seconds(when: datetime | None = None) -> datetime:
    if not when:
        when = now()
    return when.replace(second=0, microsecond=0)


type Comparable = int | float | Decimal


def _mean(
    __arg1: Comparable,
    __arg2: Comparable,
    *_args: Comparable,
) -> Comparable:
    return mean([__arg1, __arg2, *_args])


AGGREGATE_FN = {
    "max": max,
    "min": min,
    "average": mean,
}

AGGREGATE = [(x, x) for x in AGGREGATE_FN]

SENSOR_TYPE = (
    ("temp", "Temperature"),
    ("humidity", "Humidity"),
    ("ph", "pH"),
    ("ec", "EC"),
    ("dist", "Distance"),
    ("light", "Light"),
    ("CO2", "CO2"),
    ("Other", "Other"),
)


class Firmware(models.Model):
    class Meta:
        get_latest_by = "id"

    name = models.CharField(max_length=255, unique=True)
    url = models.URLField()
    active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return f"Firmware rel. {self.name}"

    @staticmethod
    def refresh_from_api(
        url_base: str = "https://gitlab.com/api/v4/projects/32160975/",
    ) -> int:
        import requests

        releases = requests.get(url_base + "releases").json()
        model_releases = {}
        for db_release in Firmware.objects.all():
            model_releases[db_release.name] = db_release
            db_release.active = False
        log.warning(model_releases)
        for release in releases:
            if release["name"] in model_releases:
                db_release = Firmware.objects.get(name=release["name"])
                db_release.active = True
                model_releases[release["name"]] = db_release
            else:
                db_release = Firmware(
                    name=release["name"],
                    url=release["assets"]["links"][0]["url"],
                    active=True,
                )
                model_releases[db_release.name] = db_release

        log.warning(model_releases)

        for db_release in model_releases.values():
            db_release.save()
        return len(model_releases)


class Logger(models.Model):
    name = models.CharField(max_length=255)
    mac = models.CharField(max_length=17, unique=True, default="00:00:00:00:00:00")
    firmware = models.ForeignKey(Firmware, on_delete=models.CASCADE, null=True)
    config = models.JSONField(blank=True, null=True)
    certificate = models.TextField(blank=True, null=True)  # todo
    private_key = models.TextField(blank=True, null=True)  # todo

    def __str__(self) -> str:
        return f"Logger {self.name}"

    def as_age_info(self) -> LoggerAgeInfo:
        record = SensorData.objects.filter(sensor__logger=self).latest()
        if not record:
            return LoggerAgeInfo(idx=self.id, name=self.name, age=None)
        return LoggerAgeInfo(idx=self.id, name=self.name, age=record.age)


class SensorBase(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=255)
    sensor_type = models.CharField(max_length=10, choices=SENSOR_TYPE)


class Sensor(SensorBase):
    @classmethod
    def by_path(cls, path) -> "Sensor":
        if isinstance(path, int) or path.isdigit():
            apparent_sensor = ApparentSensor.objects.get(index=path)
            return cls.objects.get(name__iexact=apparent_sensor.name)
        if ":" not in path:
            return cls.objects.get(name__exact=path)
        logger, sensor = path.split(":", 1)
        logger = Logger.objects.get(name__iexact=logger)
        return cls.objects.get(name__iexact=sensor, logger=logger)

    logger = models.ForeignKey(
        Logger,
        on_delete=models.CASCADE,
        related_name="sensors",
        null=True,
    )
    index = models.IntegerField(
        unique=True,
        verbose_name="index of sensor in interface",
        null=True,
        blank=True,
    )

    shift = models.FloatField("zero degree correction", default=0)
    slope = models.FloatField("first degree correction", default=1)
    lower_limit = models.FloatField(
        "Lower limit (ignore calibrated sensor values lower than this)",
        blank=True,
        null=True,
    )
    upper_limit = models.FloatField("Upper limit for logging", blank=True, null=True)

    if TYPE_CHECKING:
        routers: QuerySet["Router"]

    def log(self, value: float | None, when: datetime | None = None):
        if value is None:
            return False
        if math.isfinite(value):
            corrected_value = self.slope * value + self.shift
            if self.check_limits(corrected_value):
                when = crop_seconds(when)
                data, created = SensorData.objects.get_or_create(
                    defaults={
                        "sensor": self,
                        "time": when,
                        "value": corrected_value,
                    },
                    sensor=self,
                    time=when,
                )
                if not created:
                    data.value = corrected_value
                data.save()
                self.log_by_router(when)
                return corrected_value
        return False

    def check_limits(self, corrected_value):
        if self.lower_limit is not None and math.isfinite(self.lower_limit):
            passes_lower_limit = self.lower_limit <= corrected_value
        else:
            passes_lower_limit = True

        if self.upper_limit is not None and math.isfinite(self.upper_limit):
            passes_upper_limit = corrected_value <= self.upper_limit
        else:
            passes_upper_limit = True

        return passes_lower_limit and passes_upper_limit

    def log_by_router(self, when: datetime):
        for router in self.routers.all():
            router = cast(Router, router)
            router.log(when)

    def __str__(self):
        return f"{self.name} sensor ({self.logger.name})"

    def update_calibration(self) -> None:
        calibration_data = self.calibration_data.all().order_by("-timestamp")
        calibration_data_count = calibration_data.count()
        self.offset = 0.0
        self.slope = 1.0
        self.power = 1.0
        if calibration_data_count == 0:
            return self.save()
        if calibration_data_count == 1:
            point_1 = calibration_data[0]
            self.offset = point_1.real_value - point_1.sensor_reading
            self.slope = 1
        else:  # in case of more records, use only 2 newest (explicit ordering)
            point_1 = calibration_data[0]
            point_2 = calibration_data[1]
            try:
                # (real2-real1)/(sensor2-sensor1)
                self.slope = (point_2.real_value - point_1.real_value) / (point_2.sensor_reading - point_1.sensor_reading)
                # real - sensor * slope
                self.offset = point_1.real_value - point_1.sensor_reading * self.slope
            except ZeroDivisionError:
                self.slope = 1
                self.offset = 0

        return self.save()


class SensorDataTemplate(models.Model):
    class Meta:
        abstract = True
        verbose_name = "sensor record"
        verbose_name_plural = "sensor records"
        get_latest_by = "time"

    time = models.DateTimeField(verbose_name="time of logging")
    value = models.FloatField(verbose_name="logged value")
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE, related_name="data")

    @property
    def age(self):
        return (now() - self.time).total_seconds()


class SensorData(SensorDataTemplate):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE, related_name="data")

    def __str__(self):
        return f"Logged data from {self.sensor.name} at {self.time} with value of {self.value}"


class ApparentSensor(SensorBase):
    index = models.IntegerField(
        unique=True,
        verbose_name="index of sensor in interface",
    )

    def __str__(self):
        return f"Virtual {self.name} sensor ({self.index})"


class ApparentSensorData(SensorDataTemplate):
    sensor = models.ForeignKey(
        ApparentSensor,
        on_delete=models.CASCADE,
        related_name="data",
    )
    offline_sensors = ArrayField(models.CharField(max_length=255), default=list)

    def __str__(self):
        return f"Aggregate data from {self.sensor.name} at {self.time} with value of {self.value}"


class Router(models.Model):
    """
    Router gets data from more sensors, aggregates them and stores them in ApparentSensor
    """

    name = models.CharField(max_length=256, blank=True, null=True)
    apparent_sensor = models.ForeignKey(ApparentSensor, on_delete=models.CASCADE)
    aggregate = models.CharField(
        max_length=7,
        choices=AGGREGATE,
        verbose_name="Function to compute value",
        default="max",
    )
    sensors = models.ManyToManyField(Sensor, related_name="routers")
    last_changed_at = models.DateTimeField(auto_now=True)

    def log(self, when: datetime | None = None):
        when = crop_seconds(when)
        all_sensors = set(self.sensors.all())
        sensor_data = SensorData.objects.filter(
            sensor__in=self.sensors.all(),
            time=when,
        )
        values = [reading.value for reading in sensor_data]
        online_sensors = {data.sensor for data in sensor_data}
        offline_sensors = list(all_sensors - online_sensors)
        if values:
            apparent_value = AGGREGATE_FN[self.aggregate](values)

            apparent_data, _ = ApparentSensorData.objects.get_or_create(
                defaults={
                    "sensor": self.apparent_sensor,
                    "time": when,
                    "value": apparent_value,
                },
                sensor=self.apparent_sensor,
                time=when,
            )
            apparent_data.value = apparent_value
            apparent_data.offline_sensors = offline_sensors
            apparent_data.save()


class SensorDataHourly(SensorDataTemplate):
    sensor = models.ForeignKey(
        Sensor,
        related_name="hourly_average",
        verbose_name="sensor",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "sensor hourly average"
        verbose_name_plural = "sensor hourly averages"
        ordering = ["time", "sensor"]
        get_latest_by = "time"


class SensorDataDaily(SensorDataTemplate):
    sensor = models.ForeignKey(
        Sensor,
        related_name="daily_average",
        verbose_name="sensor",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "sensor daily average"
        verbose_name_plural = "sensor daily averages"
        ordering = ["time", "sensor"]
        get_latest_by = "time"


class ApparentSensorDataHourly(SensorDataTemplate):
    sensor = models.ForeignKey(
        ApparentSensor,
        related_name="hourly_average",
        verbose_name="sensor",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "apparent sensor hourly average"
        verbose_name_plural = "apparent sensor hourly averages"
        ordering = ["time", "sensor"]
        get_latest_by = "time"


class ApparentSensorDataDaily(SensorDataTemplate):
    sensor = models.ForeignKey(
        ApparentSensor,
        related_name="daily_average",
        verbose_name="sensor",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "apparent sensor daily average"
        verbose_name_plural = "apparent sensor daily averages"
        ordering = ["time", "sensor"]
        get_latest_by = "time"


class CalibrationData(models.Model):
    class Meta:
        ordering = ["id"]

    sensor_reading = models.FloatField(_("value read from sensor"))
    real_value = models.FloatField(_("User measured value"))
    timestamp = models.DateTimeField(_("time of measurement"), auto_now=True)

    sensor = models.ForeignKey(
        Sensor,
        related_name="calibration_data",
        on_delete=models.CASCADE,
    )

    def as_dict(self) -> dict:
        return {
            "id": self.id,
            "sensor_reading": self.sensor_reading,
            "real_value": self.real_value,
            "timestamp": self.timestamp,
            "sensor": self.sensor.id,
        }


class RawDataStore(models.Model):
    sensor = models.ForeignKey(
        Sensor,
        related_name="raw_data",
        verbose_name=_("raw data"),
        on_delete=models.CASCADE,
    )
    value = models.FloatField(_("last read raw value"), blank=True, null=True)
    measured = models.BooleanField(default=False)

from collections.abc import Mapping
import datetime
import math
from zoneinfo import ZoneInfo

from django.conf import settings
from passlib.apache import HtpasswdFile


def set_htpass(user: str, new_password: str, old_password: str) -> bool:
    ht = HtpasswdFile("/etc/nginx/.htpasswd")
    if user in ht.users() and ht.check_password(user, old_password):
        ht.set_password(user, new_password)
        ht.save()
        return True
    return False


def check_htpass(user: str, password: str) -> bool:
    ht = HtpasswdFile("/etc/nginx/.htpasswd")
    return user in ht.users() and ht.check_password(user, password)


def rename_htuser(old_username: str, new_username: str, password: str) -> None:
    ht = HtpasswdFile("/etc/nginx/.htpasswd")
    if old_username in ht.users() and ht.check_password(old_username, password):
        ht.delete(old_username)
        ht.set_password(new_username, password)
        ht.save()


def tzinfo() -> ZoneInfo:
    timezone = getattr(settings, "TIME_ZONE", "UTC")
    return ZoneInfo(timezone)


def jsonize(
    data: Mapping[int, float | None],
    amount: int,
    format_string: str = "{:.0f}",
    start: int = 0,
) -> list[str]:
    toret = []
    for i in range(start, amount):
        if i in data and data[i] is not None and not math.isnan(data[i]):
            to_append = data[i]
            if isinstance(to_append, float):
                to_append = format_string.format(to_append)
            toret.append(to_append)
        else:
            from .config import MINVALUE

            toret.append(MINVALUE)
    return toret


def crop_seconds(now: datetime.datetime | None = None) -> datetime.datetime:
    if now is None:
        now = datetime.datetime.now(tzinfo())

    return datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, tzinfo=tzinfo())


def get_daymin(now=None) -> int:
    if now is None:
        now = datetime.datetime.now(tzinfo())
    return now.minute + 60 * now.hour


def to_tz(tz):
    try:
        tznum = int(tz) * -1
        if tznum > 0:
            tz = f"Etc/GMT+{tznum}"
        elif tznum == 0:
            tz = "UTC"
        else:
            tz = f"Etc/GMT{tznum}"
    except ValueError:
        pass
    return tz


def get_mac(iface: str) -> str:
    try:
        with open(f"/sys/class/net/{iface}/address") as f:
            return f.readlines()[0].strip()
    except OSError:
        return "de:ab:be:ef:00:01"


def netmask_to_cidr(
    m_netmask,
):  # https://gist.github.com/Akendo/6cf70aa01f92ab2f03ae6c27480f713e
    return sum([bin(int(bits)).count("1") for bits in m_netmask.split(".")])


def flags_to_security(flags, wpa_flags, rsn_flags):
    # try:
    #     import gi
    #
    #     gi.require_version("NM", "1.0")
    #     from gi.repository import NM
    #
    #     privflag = getattr(NM, "80211ApFlags").PRIVACY
    #     keymgmt = getattr(NM, "80211ApSecurityFlags").KEY_MGMT_802_1X
    # except (ImportError, ValueError):
    privflag = 1
    keymgmt = 512

    toret = ""
    if (flags & privflag) and (wpa_flags == 0) and (rsn_flags == 0):
        toret = toret + " WEP"
    if wpa_flags != 0:
        toret = toret + " WPA1"
    if rsn_flags != 0:
        toret = toret + " WPA2"
    if (wpa_flags & keymgmt) or (rsn_flags & keymgmt):
        toret = toret + " 802.1X"
    return toret.lstrip()


def channelate(freq):
    fct = {
        2412: 1,
        2417: 2,
        2422: 3,
        2427: 4,
        2432: 5,
        2437: 6,
        2442: 7,
        2447: 8,
        2452: 9,
        2457: 10,
        2462: 11,
        2467: 12,
        2472: 13,
        5160: 32,
        5170: 34,
        5180: 36,
        5190: 38,
        5200: 40,
        5210: 42,
        5220: 44,
        5230: 46,
        5240: 48,
        5250: 50,
        5260: 52,
        5270: 54,
        5280: 56,
        5290: 58,
        5300: 60,
        5310: 62,
        5320: 64,
        5340: 68,
        5480: 96,
        5500: 100,
        5510: 102,
        5520: 104,
        5530: 106,
        5540: 108,
        5550: 110,
        5560: 112,
        5570: 114,
        5580: 116,
        5590: 118,
        5600: 120,
        5610: 122,
        5620: 124,
        5630: 126,
        5640: 128,
        5660: 132,
        5670: 134,
        5680: 136,
        5690: 138,
        5700: 140,
        5710: 142,
        5720: 144,
        5745: 149,
        5755: 151,
        5765: 153,
        5775: 155,
        5785: 157,
        5795: 159,
        5805: 161,
        5825: 165,
        5845: 169,
        5865: 173,
    }

    if freq in fct:
        return fct[freq]
    return -1


def nm_conn_clear(connection_name):
    """FIXME: ven s tím
    for c in NetworkManager.Settings.Connections:
        if c.GetSettings()["connection"]["id"] == connection_name:
            c.Delete()
    """


def wifi_clear() -> None:
    nm_conn_clear("grdw-from-web")


def wifi_start(ssid: str, password: str | None) -> None:
    wifi_clear()
    conn = {
        "802-11-wireless": {
            "mode": "infrastructure",
            "security": "802-11-wireless-security",
            "ssid": ssid,
        },
        "802-11-wireless-security": {
            "auth-alg": "open",
            "key-mgmt": "wpa-psk",
            "psk": password,
        },
        "ipv4": {"method": "auto"},
        "ipv6": {"method": "auto"},
        "connection": {"id": "grdw-from-web"},
    }
    if not password:
        conn.pop("802-11-wireless-security")


#    try:
#        NetworkManager.Settings.AddConnection(conn)
#    except DBusException:
#        pass


def eth_clear():
    nm_conn_clear("grdw-ethernet")


def eth_setup() -> None:
    from .models import get_config_field

    c = get_config_field()
    if c.get("use_dhcp", True):
        # eth_dhcp()
        pass
    else:
        # eth_static(c)
        pass
    nm_conn_clear("Wired connection 1")

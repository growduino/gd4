import datetime
from enum import Enum
import json
import logging
from typing import Any, cast

from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail.backends.smtp import EmailBackend
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from logger.models import (
    ApparentSensor as LoggerApparentSensor,
    Sensor as LoggerSensor,
    SensorData,
)

from .utils import crop_seconds, get_daymin, get_mac, tzinfo

# Create your models here.

STATES = (
    (0, False),
    (1, True),
    (2, "Always on"),
)

logger = logging.getLogger(__name__)


def get_config_field(field: str | None = None, filename: str = "config") -> dict[str, Any] | Any:
    try:
        config_db = JsonStorage.objects.filter(filename=filename).latest()
        config = cast(dict[str, Any], json.loads(config_db.value))
    except ObjectDoesNotExist:
        config = new_config(filename)

    if filename == "config":
        config["mac"] = get_mac("eth0")
    if field is None:
        try:
            tz = int(config["time_zone"])
            tz_time_delta = datetime.timedelta(hours=tz)
            tz_object = datetime.timezone(tz_time_delta, name="CUST")
            current_time = datetime.datetime.now(tz_object)
            config["current_time"] = current_time.isoformat()
        except KeyError:
            pass
        return config
    if field in config:
        return config[field]
    return {}


def new_config(filename) -> dict[str, Any]:
    if filename == "config":
        config = json.loads(
            """{
    "use_dhcp": 1,
    "mac": "",
    "ip": "192.168.1.101",
    "netmask": "255.255.255.0",
    "gateway": "192.168.1.1",
    "ntp": "cz.pool.ntp.org",
    "smtp": "",
    "mail_from": "",
    "sys_name": "Growduino",
    "smtp_port": "",
    "time_zone": "1"
}"""
        )
        config["mac"] = get_mac("eth0")
    else:
        config = {}
    return config


def save_config(config: dict, filename="config") -> None:
    """saves dict config into database."""
    if "current_time" in config:
        config.pop("current_time")

    for sensor in LoggerSensor.objects.all():
        sensor.update_calibration()

    config_str = json.dumps(config)

    config_db = JsonStorage(value=config_str, filename=filename)
    config_db.save()


def update_config(key, val):
    config = get_config_field()
    if config[key] != val:
        config[key] = val
        save_config(config)
        return True
    return False


class JsonStorage(models.Model):
    class Meta:
        verbose_name = _("configuration")
        verbose_name_plural = _("configurations")
        ordering = ["time"]
        get_latest_by = "time"

    def save(self, *args: Any, **kwargs: Any) -> None:
        """On save, update timestamp"""
        self.time = timezone.now()
        return super().save(*args, **kwargs)

    value = models.CharField(verbose_name=_("serialized config"), max_length=2048)
    filename = models.CharField(verbose_name=_("file name"), max_length=2048)
    time = models.DateTimeField()


class StateLoggedModel(models.Model):
    @property
    def state(self):
        """Returns state according to database"""
        return self.get_state()

    @property
    def uptime(self):
        """Returns time since last change with 60s granularity, false when there are no changes in database"""
        try:
            latest_record = self.log.latest()
            return crop_seconds() - latest_record.time
        except ObjectDoesNotExist:
            return False

    def get_state(self, time: datetime.datetime | None = None) -> bool:
        try:
            if time is None:
                log_record = self.log.latest()
            else:
                log_record = self.log.filter(time__lte=time).latest()
            return log_record.state
        except ObjectDoesNotExist:
            return False


class OutputRec(StateLoggedModel):
    name = models.CharField(verbose_name=_("output name"), max_length=32)
    index = models.IntegerField(unique=True, verbose_name=_("index of output in interface"))
    target_index = models.IntegerField(
        verbose_name=_("index of output on target, if different from index"),
        blank=True,
        null=True,
    )
    disabled = models.BooleanField(default=False)

    # Functions
    def __str__(self) -> str:
        return _("output #%s - %s") % (self.index, self.name)

    def switch(self, new_state):
        """If new_state is different from self.state or there is no state recorded, log new_state to database"""
        if new_state != self.state or self.uptime is False:
            logged_record = OutputLog(output=self, state=new_state)
            logged_record.save()
        return new_state

    def tick(self) -> bool | None:
        disable = False
        new_state = False

        if self.trigger.filter(active=2):  # always
            return self.switch(True)
        for trigger in self.trigger.filter(active=1):  # by condition
            if not trigger.important:  # triggers with ! do not directly affect output state
                new_state = new_state or trigger.state

            if trigger.important and trigger.on_time() and not trigger.state:
                disable = True

        if disable:
            self.disabled = True
            ret = self.switch(False)
            self.save()
            return ret
        self.disabled = False
        self.save()

        if not disable:
            if self.state is False and new_state is True:
                return self.switch(True)
            if self.state is True and new_state is False:
                return self.switch(False)
        return None

    class Meta:
        verbose_name = _("output")
        verbose_name_plural = _("outputs")
        ordering = ["index"]


MAX_VALID_AGE = 120


class TriggerActiveState(Enum):
    NEVER = 0
    ACTIVE = 1
    ALWAYS = 2


trigger_active_state = [(x.value, x.name) for x in TriggerActiveState]


class Trigger(StateLoggedModel):
    #    """    {
    #        "t_since":-1,
    #        "t_until":0,
    #        "on_value":"<250",
    #        "off_value":">300",
    #        "sensor":1,
    #        "output":7,
    #        "active":1
    #    }
    #    """

    @property
    def important(self) -> bool:
        """Importance is noted by "!" at end of off condition,
        and means that this condition is critical,
        if off condition is true it disables that output until on condition is met"""
        try:
            return self.off_value[-1] == "!"
        except IndexError:
            return False

    @property
    def sensor_val(self) -> float | None:
        """Reads value from sensor checked by this trigger"""
        assert self.sensor is not None
        if self.sensor.index == -1:
            return None
        try:
            sensor_val = cast(SensorData, self.sensor.data.latest())
        except ObjectDoesNotExist:
            return None

        if sensor_val is None or sensor_val.value is None or sensor_val.age > MAX_VALID_AGE:
            # We do not have good, recent value
            return None
        return sensor_val.value

    def switch(self, new_state: bool, forced: bool = False) -> bool:
        """change state to new_state and log to database"""
        # print("state", self.state, "new", new_state)
        if (new_state != self.state) or forced:
            record, created = TriggerLog.objects.get_or_create(trigger=self, time=crop_seconds(), defaults={"state": new_state})
            record.state = new_state
            record.save()
            return True
        return False

    def on_time(self) -> bool:
        """checks if the trigger should be ticking at this time of day"""
        daymin = get_daymin()

        if self.t_since == -1:
            return True

        if self.t_until < self.t_since:  # over midnight
            return (self.t_until > daymin) or (self.t_since < daymin)
        return self.t_since <= daymin < self.t_until

    @property
    def on_cmp(self) -> str:
        return self.on_value[0].lower()

    @property
    def on_val(self) -> float:
        return float(self.on_value[1:])

    @property
    def off_val(self) -> float:
        off_val = self.off_value[1:]
        if self.important:
            off_val = off_val[:-1]
        return float(off_val)

    @property
    def off_cmp(self) -> str:
        return self.off_value[0].lower()

    def tick(self, forced: bool = False) -> bool:
        """Checks trigger conditions, sets state. Returns true on changing state
        on forced saves state even if not changing"""

        if self.active == TriggerActiveState.NEVER:
            return self.switch(False, forced)

        if self.active == TriggerActiveState.ALWAYS:
            return self.switch(True, forced)

        sensor_value = self.sensor_val

        if not self.on_time():
            return self.switch(False, forced)

        wanted_state = False  # True means we want to switch output on

        if not self.state:  # trigger was off during last tick()
            logger.debug("trying to turn on")

            if self.on_cmp == "t":  # enable after X minutes of inactivity
                if self.output:
                    wanted_state = (self.output.state is False) and (
                        self.output.uptime is False or self.output.uptime.seconds >= (self.on_val * 60)
                    )
            elif self.on_cmp == "<" and sensor_value is not None:
                wanted_state = sensor_value < self.on_val
            elif self.on_cmp == ">" and sensor_value is not None:
                wanted_state = sensor_value > self.on_val
            elif self.on_cmp == "c":
                if self.output:
                    output = OutputRec.objects.get(id=self.output.id)
                    wanted_state = output.uptime > self.off_val * 60
                else:
                    wanted_state = False

        else:  # trigger was on.
            logger.debug("trying to turn off")

            if self.off_cmp == "t":
                # print("t", self.off_val, self.output.uptime.seconds)
                if self.output and self.output.state:
                    wanted_state = self.output.uptime.seconds < (self.off_val * 60)
            elif self.off_cmp == "<" and sensor_value is not None:
                wanted_state = sensor_value >= self.off_val
            elif self.off_cmp == ">" and sensor_value is not None:
                wanted_state = sensor_value <= self.off_val
            elif self.off_cmp == "c":
                if self.output:
                    output = OutputRec.objects.get(id=self.output.id)
                    wanted_state = not (not output.state and output.uptime < self.off_val * 60)
                else:
                    wanted_state = False
            # print(wanted_state)

        # print("result is", wanted_state)
        return self.switch(wanted_state, forced)

    def as_dict(self) -> dict[str, str | int | float]:
        result: dict[str, str | int | float] = {
            "t_since": self.t_since,
            "t_until": self.t_until,
            "on_value": self.on_value,
            "off_value": self.off_value,
        }
        assert self.sensor is not None
        result["sensor"] = int(self.sensor.index)
        assert self.output is not None
        result["output"] = int(self.output.index)
        result["active"] = self.active
        return result

    index = models.IntegerField(unique=True, verbose_name=_("trigger ID"))
    t_since = models.IntegerField(verbose_name=_("trigger start time"), default=-1)
    t_until = models.IntegerField(verbose_name=_("trigger end time"), default=-1)
    on_value = models.CharField(verbose_name=_("on condition and value"), max_length=20)
    off_value = models.CharField(verbose_name=_("off condition and value"), max_length=20)
    sensor = models.ForeignKey(
        LoggerApparentSensor,
        null=True,
        related_name="trigger",
        verbose_name=_("sensor"),
        on_delete=models.CASCADE,
    )
    output = models.ForeignKey(
        OutputRec,
        null=True,
        related_name="trigger",
        verbose_name=_("controlled output"),
        on_delete=models.CASCADE,
    )
    active = models.IntegerField(
        verbose_name=_("is trigger active?"),
        choices=trigger_active_state,
        default=TriggerActiveState.NEVER,
    )

    def __str__(self) -> str:
        if self.active == TriggerActiveState.ACTIVE:
            act = "active"
        elif self.active == TriggerActiveState.ALWAYS:
            act = "always on"
        else:
            act = "disabled"
        return _("%s trigger %s sensor %s output %s") % (
            act,
            self.index,
            self.sensor,
            self.output,
        )

    class Meta:
        verbose_name = _("trigger rule")
        verbose_name_plural = _("trigger rules")
        ordering = ["index"]


class Alert(models.Model):
    #        """
    #  {
    #      "on_message":"Temp too high!",
    #      "off_message":"Temp back at normal",
    #      "trigger":1,
    #      "target":"+420777123456",
    #  }
    # """

    index = models.IntegerField(verbose_name=_("alert ID"), unique=True)
    on_message = models.CharField(verbose_name=_("message to send on alarm start"), max_length=1000)
    off_message = models.CharField(verbose_name=_("message to send on alarm end"), max_length=1000)
    trigger = models.ForeignKey(
        Trigger,
        related_name="alert",
        verbose_name=_("alert"),
        on_delete=models.CASCADE,
    )
    target = models.CharField(verbose_name=_("message destination"), max_length=200)
    active = models.IntegerField(verbose_name=_("is alert active?"), default=0)

    def __str__(self) -> str:
        return _("Alert on trigger %s") % (self.trigger.index,)

    def process_alert(self) -> None:
        """sends alert if its trigger has changed"""
        if not self.active:
            return

        try:
            last_log = AlertLog.objects.filter(alert=self).latest()
        except ObjectDoesNotExist:
            last_log = None

        if (last_log is None and self.trigger.state is True) or (last_log is not None and last_log.state != self.trigger.state):
            new_log = AlertLog(
                # trigger=self.trigger,
                alert=self,
                state=self.trigger.state,
            )

            if self.trigger.state:
                subject = "[{sys_name}] alert".format(sys_name=get_config_field("sys_name"))
                body = self.on_message
            else:
                body = self.off_message
                subject = "[{sys_name}] end of alert".format(sys_name=get_config_field("sys_name"))

            assert self.trigger.sensor is not None
            val = self.trigger.sensor.data.latest().value
            body = body + f"\nValue: {val:.2f}"

            if "," in self.target:
                target = self.target.split(",")
            else:
                target = [self.target]

            new_log.save()

            host = get_config_field("smtp")
            port = get_config_field("smtp_port")
            username = get_config_field("smtp_user")
            password = get_config_field("smtp_pwd")
            use_tls = get_config_field("smtp_ssl") == "tls"
            use_ssl = get_config_field("smtp_ssl") == "ssl"

            backend = EmailBackend(
                host=host,
                port=port,
                username=username,
                password=password,
                use_ssl=use_ssl,
                use_tls=use_tls,
                timeout=30,
            )
            mail.EmailMessage(
                subject=subject,
                body=body,
                from_email=str(get_config_field("mail_from")),
                to=target,
                connection=backend,
            ).send()

    class Meta:
        verbose_name = _("alert rule")
        verbose_name_plural = _("alert rules")
        ordering = ["index"]


class TriggerLog(models.Model):
    def save(self, *args: Any, **kwargs: Any) -> None:
        """On save, update timestamp"""
        self.time = crop_seconds()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("trigger change log")
        verbose_name_plural = _("trigger change logs")
        ordering = ["-time"]
        get_latest_by = "time"

    def __str__(self) -> str:
        if self.state:
            act = "revive"
        else:
            act = "disable"
        return _("%s from trg %s at %s") % (act, self.trigger.index, self.time)

    trigger = models.ForeignKey(
        Trigger,
        related_name="log",
        verbose_name=_("log"),
        on_delete=models.CASCADE,
    )
    state = models.BooleanField()
    time = models.DateTimeField()


class OutputLog(models.Model):
    def save(self, *args: Any, **kwargs: Any) -> None:
        """On save, update timestamp"""
        self.time = crop_seconds()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("output change log")
        verbose_name_plural = _("output change logs")
        ordering = ["time"]
        get_latest_by = "time"

    output = models.ForeignKey(
        OutputRec,
        related_name="log",
        verbose_name=_("log"),
        on_delete=models.CASCADE,
    )
    state = models.BooleanField()
    time = models.DateTimeField()


class AlertLog(models.Model):
    def save(self, *args: Any, **kwargs: Any) -> None:
        """On save, update timestamp"""
        self.time = crop_seconds()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("alert log")
        verbose_name_plural = _("alert logs")
        ordering = ["time"]
        get_latest_by = "time"

    alert = models.ForeignKey(
        Alert,
        related_name="log",
        verbose_name=_("log"),
        on_delete=models.CASCADE,
    )
    state = models.BooleanField()
    time = models.DateTimeField()


class EventTimes(models.Model):
    event = models.CharField(max_length=100)
    time = models.DateField()


class LcdLine(models.Model):
    class Meta:
        ordering = ["priority"]
        verbose_name = _("line to display on lcd")
        verbose_name_plural = _("line to display on lcd")

    def display(self) -> str:
        sensor = self.sensor if self.sensor else self.apparent_sensor
        out = self.text
        if self.is_datetime:
            now = datetime.datetime.now(tzinfo())
            out = now.strftime("%Y-%m-%d %H:%M")
        else:
            try:
                if "{val" in self.text:
                    reading = sensor.data.latest()
                    if not reading or (self.error_age > 0 and reading.age > (self.error_age * 60)):
                        out = self.error_text
                    else:
                        val = sensor.data.latest().value
                        out = self.text.format(val=val)
            except KeyError:
                pass
            except ObjectDoesNotExist:
                return self.error_text
        return out

    priority = models.IntegerField(_("order lower first"), unique=True)
    text = models.CharField(_("text to display, use {val} to read sensor"), max_length=100, blank=True)
    error_text = models.CharField(
        _("Text to display if last sensor read is older than age minutes"),
        max_length=100,
        blank=True,
    )
    error_age = models.IntegerField(_("Age of last reading to be declared faulty, 0 to disable"), default=0)
    sensor = models.ForeignKey(
        LoggerSensor,
        related_name="lcd_lines",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    apparent_sensor = models.ForeignKey(
        LoggerApparentSensor,
        related_name="lcd_lines",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    is_datetime = models.BooleanField(_("check to display clock and not text"), default=False)

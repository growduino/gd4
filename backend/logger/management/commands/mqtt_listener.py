import json
import logging
from typing import Any, cast

from django.conf import settings
from django.core.management import BaseCommand
import paho.mqtt.client as mqtt

from logger.models import Logger, RawDataStore
from logger.rest import LogRecord

log = logging.getLogger(__name__)


def on_connect(
    client: mqtt.Client,
    userdata: Any,
    flags: Any,
    rc: Any,
    props: Any,
) -> None:
    log.debug("on_connect")
    client.subscribe("log/#")
    log.debug("subscribed to log/#")


def on_message(
    client: mqtt.Client,
    userdata: Any,
    msg: mqtt.MQTTMessage,
) -> None:
    log.debug("Received message in topic %s: %s", msg.topic, msg.payload)

    path, rest = msg.topic.split("/", maxsplit=1)
    if path == "log":
        try:
            if rest == "internal":
                logger = Logger.objects.get(name="internal")
            else:
                logger = Logger.objects.get(id=rest)
            log.debug("storing records for logger %s", logger)
        except Logger.DoesNotExist:
            log.error("no logger found for %s", rest)
            log.debug("Available loggers: %s", ", ".join(logger.name for logger in Logger.objects.all()))
            return
        if msg and msg.payload:
            try:
                received_data = json.loads(msg.payload)
            except json.decoder.JSONDecodeError:
                log.error("invalid JSON payload: %s", msg.payload)
                return
            if "rawvalue" in received_data:
                sensor = logger.sensors.get(name__iexact=received_data["sensor"])
                raw_data, created = RawDataStore.objects.get_or_create(sensor=sensor)
                raw_data.value = received_data["rawvalue"]
                raw_data.measured = True
                raw_data.save()
            else:
                for record in received_data:
                    cast(LogRecord, record)
                    log.debug("storing record: %s", record)
                    logger.sensors.get(name=record["name"]).log(record["value"])


class Command(BaseCommand):
    help = "Subscribe to logger topics and store data in database."
    requires_migrations_checks = True

    _client: mqtt.Client
    _connected: bool = False

    def setup(self) -> None:
        self._client = mqtt.Client(
            client_id="django-mqtt-listener",
            callback_api_version=mqtt.CallbackAPIVersion.VERSION2,  # type: ignore[attr-defined]
            clean_session=True,
        )
        self._client.on_connect = on_connect
        self._client.on_message = on_message
        self._connected = False

    def handle(self, *_args: Any, **options: Any) -> None:
        self.setup()
        self._client.connect(
            host=settings.RMQTT_HOST,
            port=1883,
            keepalive=60,
        )
        log.info("Connected to MQTT broker, starting loop")
        self._client.loop_forever()

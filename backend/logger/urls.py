from django.urls import include, path

from logger.rest import logger_router, router

urlpatterns = [
    path("", include(router.urls)),
    path("", include(logger_router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]

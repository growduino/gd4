from typing import Any, TypedDict

from django.core.management.base import BaseCommand

from core.models import LcdLine
from logger.models import ApparentSensor


class LcdLineTemplate(TypedDict):
    apparent_sensor: str | None
    error_age: int
    error_text: str
    is_datetime: bool
    priority: int
    text: str


lcd_lines: list[LcdLineTemplate] = [
    {
        "apparent_sensor": None,
        "error_age": 0,
        "error_text": "",
        "is_datetime": False,
        "priority": 1,
        "text": "Hello Grower!",
    },
    {
        "apparent_sensor": "Temp1",
        "error_age": 2,
        "error_text": "DHT22 temp read err!",
        "is_datetime": False,
        "priority": 3,
        "text": "Temperature {val:.1f}°C",
    },
    {
        "apparent_sensor": "Humidity",
        "error_age": 2,
        "error_text": "DHT22 hum read err!",
        "is_datetime": False,
        "priority": 5,
        "text": "Humidity {val:.0f}%",
    },
    {
        "apparent_sensor": "Temp2",
        "error_age": 2,
        "error_text": "WaterTemp read err!",
        "is_datetime": False,
        "priority": 7,
        "text": "Water temp {val:.1f}°C",
    },
    {
        "apparent_sensor": "Temp3",
        "error_age": 2,
        "error_text": "BulbTemp read err!",
        "is_datetime": False,
        "priority": 9,
        "text": "Bulb temp {val:.1f}°C",
    },
    {
        "apparent_sensor": "Usnd",
        "error_age": 2,
        "error_text": "USND read err!",
        "is_datetime": False,
        "priority": 11,
        "text": "Water level {val:.0f}cm",
    },
    {
        "apparent_sensor": "EC",
        "error_age": 2,
        "error_text": "EC read err!",
        "is_datetime": False,
        "priority": 13,
        "text": "EC {val:.2f}",
    },
    {
        "apparent_sensor": "pH",
        "error_age": 2,
        "error_text": "pH read err!",
        "is_datetime": False,
        "priority": 15,
        "text": "pH {val:.2f}",
    },
    {
        "apparent_sensor": "CO2",
        "error_age": 2,
        "error_text": "CO2 read err!",
        "is_datetime": False,
        "priority": 17,
        "text": "CO2 {val:.0f}ppm",
    },
    {
        "apparent_sensor": "Battery",
        "error_age": 2,
        "error_text": "Battery read err!",
        "is_datetime": False,
        "priority": 19,
        "text": "Battery {val}%",
    },
]


class Command(BaseCommand):
    help = "Create default set of lcd lines, if there is none. Use --reset to delete and recreate them."
    requires_migrations_checks = True

    def handle(self, *_args: Any, **options: Any) -> None:
        if "reset" in options and options["reset"]:
            print("Deleting lcd lines")  # noqa: T201
            LcdLine.objects.all().delete()

        if LcdLine.objects.all().count() == 0:
            print("Creating default lcd lines")  # noqa: T201
            for line in lcd_lines:
                print(f"Creating lcd line for {line['apparent_sensor']}")  # noqa: T201
                LcdLine.objects.create(
                    error_age=line["error_age"],
                    error_text=line["error_text"],
                    is_datetime=line["is_datetime"],
                    priority=line["priority"],
                    apparent_sensor=ApparentSensor.objects.get(name=line["apparent_sensor"])
                    if line["apparent_sensor"]
                    else None,
                    text=line["text"],
                )
        else:
            print("There are some already...")  # noqa: T201

    def add_arguments(self, parser) -> None:
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete current lcd lines and create new ones.",
        )

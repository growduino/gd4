import json
import os
import sys

import psycopg2

POSTGRES_USER = os.environ.get("POSTGRES_USER")
POSTGRES_PASSWORD = os.environ.get("POSTGRES_PASSWORD")
POSTGRES_DB = os.environ.get("POSTGRES_DB")
POSTGRES_HOST = os.environ.get("POSTGRES_HOST")


def to_tz(tz):
    try:
        tznum = int(tz) * -1
        if tznum > 0:
            tz = f"Etc/GMT+{tznum}"
        elif tznum == 0:
            tz = "UTC"
        else:
            tz = f"Etc/GMT{tznum}"
    except ValueError:
        tz = "UTC"
    return tz


try:
    conn = psycopg2.connect(
        f"dbname='{POSTGRES_DB}' user='{POSTGRES_USER}' host='{POSTGRES_HOST}' password='{POSTGRES_PASSWORD}'"
    )
except Exception as e:
    print(f"I am unable to connect to the database: {e}")
    sys.exit(1)

sql = "SELECT value FROM core_jsonstorage where filename = 'config' order by time desc "
with conn.cursor() as cursor:
    cursor.execute(sql)
    single_row = cursor.fetchone()
    if not single_row:
        print("UTC")
        sys.exit(0)
    value = json.loads(single_row[0]).get("time_zone", "UTC")
    print(to_tz(value))

from logger.const import APPARENT_SENSORS
from logger.models import ApparentSensor


def run():
    ApparentSensor.objects.all().delete()
    for name, sensor_type, index in APPARENT_SENSORS:
        sensor = ApparentSensor(name=name, sensor_type=sensor_type, index=index)
        sensor.save()

#!/bin/sh

poetry run mypy --strict ${@:-.}

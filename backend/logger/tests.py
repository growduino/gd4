from datetime import datetime
import json
from math import isfinite
from unittest.mock import ANY

from django.test import Client
import pytest

from logger.const import INTERNAL_SENSORS
from logger.models import (
    ApparentSensor,
    ApparentSensorData,
    Logger,
    Router,
    Sensor,
    SensorData,
)

HTTP_200_OK = 200
HTTP_201_CREATED = 201

SHIFT = 3
SLOPE = 2
MEASURED_VALUE = 10


@pytest.fixture()
def test_time():
    return datetime.fromisoformat("2022-03-01T11:00:00+01:00")


@pytest.fixture()
def sample_logger():
    logger = Logger(name="test logger")
    logger.save()
    return logger


@pytest.fixture()
def sample_other_logger():
    logger = Logger(name="test logger too", mac="00:00:00:00:00:01")
    logger.save()
    return logger


@pytest.fixture()
def sample_sensor(sample_logger):
    sensor1 = Sensor(
        name="test sensor 1",
        logger=sample_logger,
        sensor_type="temp",
    )
    sensor1.save()
    return sensor1


@pytest.fixture()
def sample_other_sensor(sample_other_logger):
    sensor2 = Sensor(
        name="other test sensor",
        logger=sample_other_logger,
        sensor_type="temp",
    )
    sensor2.save()
    return sensor2


@pytest.fixture()
def apparent_sensor():
    a_sensor = ApparentSensor(
        name="Temp1",
        sensor_type="temp",
        index=1,
    )
    a_sensor.save()
    return a_sensor


@pytest.fixture()
def sensor_router(sample_sensor, apparent_sensor):
    rt = Router(
        apparent_sensor=apparent_sensor,
        aggregate="min",
    )
    rt.save()
    rt.sensors.add(sample_sensor)
    return rt


@pytest.fixture()
def _sensor_router_two_sensors(sample_sensor, sample_other_sensor, apparent_sensor):
    rt = Router(
        apparent_sensor=apparent_sensor,
        aggregate="average",
    )
    rt.save()
    rt.sensors.add(sample_sensor)
    rt.sensors.add(sample_other_sensor)


@pytest.fixture()
def _full_sensor_suite(sample_logger):
    for name, type_, lower, upper, index in INTERNAL_SENSORS:
        sensor = Sensor(
            id=index,
            name=name,
            logger=sample_logger,
            sensor_type=type_,
            lower_limit=lower,
            upper_limit=upper,
            slope=1,
        )
        sensor.save()


@pytest.mark.django_db()
def test_create_sensors():
    logger = Logger(name="test logger")
    logger.save()

    sensor1 = Sensor(
        name="test sensor 1",
        logger=logger,
        sensor_type="ph",
    )
    sensor1.save()

    sensor2 = Sensor(
        name="test sensor 2",
        logger=logger,
        sensor_type="ec",
    )
    sensor2.save()

    assert sensor1 in logger.sensors.all()


@pytest.mark.django_db()
def test_simple_log(test_time, sample_sensor):
    measured_value = 10
    sample_sensor.log(measured_value, test_time)

    logged_values = SensorData.objects.all()
    assert logged_values

    logged_value = logged_values[0]

    assert logged_value.value == measured_value
    assert logged_value.time == test_time
    c = Client()
    response = c.get(f"/loggers/logger/{sample_sensor.logger.id}/")
    assert response.status_code == HTTP_200_OK
    json_response = json.loads(response.content)
    assert json_response == {
        "config": None,
        "id": 2,
        "mac": "00:00:00:00:00:00",
        "name": "test logger",
        "firmware": None,
        "sensors": "http://testserver/loggers/logger/2/sensor/",
    }


@pytest.mark.django_db()
def test_log_limits(test_time, sample_sensor):
    sample_sensor.upper_limit = 100
    sample_sensor.log(999, test_time)

    logged_values = SensorData.objects.all()
    assert not logged_values


@pytest.mark.django_db()
def test_log_limits_lower(test_time, sample_sensor):
    sample_sensor.lower_limit = 0
    sample_sensor.log(-999, test_time)

    logged_values = SensorData.objects.all()
    assert not logged_values


@pytest.mark.django_db()
def test_log_calib_shift(test_time, sample_sensor):
    sample_sensor.shift = SHIFT
    sample_sensor.log(MEASURED_VALUE, test_time)
    logged_values = SensorData.objects.all()
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE + SHIFT


@pytest.mark.django_db()
def test_log_calib_slope(test_time, sample_sensor):
    sample_sensor.slope = SLOPE
    sample_sensor.log(MEASURED_VALUE, test_time)
    logged_values = SensorData.objects.all()
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE * SLOPE


@pytest.mark.django_db()
def test_log_to_router(
    test_time,
    sensor_router,  # noqa: ARG001
    sample_sensor,
):
    sample_sensor.log(MEASURED_VALUE, test_time)
    logged_values = ApparentSensorData.objects.all()
    assert logged_values
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE
    assert logged_value.offline_sensors == []


@pytest.mark.django_db()
@pytest.mark.usefixtures("_sensor_router_two_sensors")
def test_log_to_router_with_two_sensors(
    test_time,
    sample_sensor,
):
    sample_sensor.log(MEASURED_VALUE, test_time)
    logged_values = ApparentSensorData.objects.all()
    assert logged_values.count() == 1
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE
    assert logged_value.offline_sensors == ["other test sensor sensor (test logger too)"]


@pytest.mark.django_db()
@pytest.mark.usefixtures("_sensor_router_two_sensors")
def test_log_to_router_with_two_sensors_two_values(
    test_time,
    sample_sensor,
    sample_other_sensor,
):
    sample_sensor.log(MEASURED_VALUE, test_time)
    sample_other_sensor.log(MEASURED_VALUE + 2, test_time)
    logged_values = ApparentSensorData.objects.all()
    assert logged_values.count() == 1
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE + 1
    assert logged_value.offline_sensors == []


@pytest.mark.django_db()
def test_log_to_router_2_min(test_time, sensor_router, sample_sensor, sample_logger):
    sensor2 = Sensor(
        name="test sensor 2",
        logger=sample_logger,
        sensor_type="temp",
    )
    sensor2.save()
    sensor_router.sensors.add(sensor2)
    sample_sensor.log(MEASURED_VALUE, test_time)
    sensor2.log(MEASURED_VALUE - 2, test_time)
    logged_values = ApparentSensorData.objects.all()
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE - 2


@pytest.mark.django_db()
def test_log_to_router_2_max(test_time, sensor_router, sample_sensor, sample_logger):
    sensor_router.aggregate = "max"
    sensor_router.save()
    sensor2 = Sensor(
        name="test sensor 2",
        logger=sample_logger,
        sensor_type="temp",
    )
    sensor2.save()
    sensor_router.sensors.add(sensor2)
    sample_sensor.log(MEASURED_VALUE, test_time)
    sensor2.log(MEASURED_VALUE - 2, test_time)
    logged_values = ApparentSensorData.objects.all()
    assert logged_values.count() == 1
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE


@pytest.mark.django_db()
def test_log_to_router_2_avg(test_time, sensor_router, sample_sensor, sample_logger):
    sensor_router.aggregate = "average"
    sensor_router.save()
    sensor2 = Sensor(
        name="test sensor 2",
        logger=sample_logger,
        sensor_type="temp",
    )
    sensor2.save()
    sensor_router.sensors.add(sensor2)
    sample_sensor.log(MEASURED_VALUE, test_time)
    sensor2.log(MEASURED_VALUE - 2, test_time)
    logged_values = ApparentSensorData.objects.all()
    logged_value = logged_values[0]
    assert logged_value.value == MEASURED_VALUE - 1


@pytest.mark.django_db()
def test_sensor_by_name(apparent_sensor, sample_sensor):  # noqa: ARG001
    assert Sensor.by_path("test sensor 1") == sample_sensor
    assert Sensor.by_path("test logger:test sensor 1") == sample_sensor


@pytest.mark.django_db()
def test_sensor_by_index(apparent_sensor, sample_sensor):
    apparent_sensor.name = "test sensor 1"
    apparent_sensor.save()
    assert Sensor.by_path(1) == sample_sensor


@pytest.mark.django_db()
def test_external_sensor_by_name(sample_sensor):
    logger = Logger(name="box", mac="00:00:00:00:00:02")
    logger.save()
    sample_sensor.logger = logger
    sample_sensor.save()
    assert Sensor.by_path("box:test sensor 1") == sample_sensor


@pytest.mark.freeze_time("2022-03-01T11:00:00+01:00")
@pytest.mark.django_db()
def test_log_something(sample_sensor):
    sample_sensor.log(1)
    c = Client()
    url = f"/loggers/logger/{sample_sensor.logger.id}/sensor/"
    response = c.get(url)
    assert response.status_code == HTTP_200_OK

    wanted_response = [
        {
            "id": sample_sensor.id,
            "name": "test sensor 1",
            "sensor_type": "temp",
            "logger": f"http://testserver/loggers/logger/{sample_sensor.logger.id}/",
            "shift": 0.0,
            "slope": 1.0,
            "lower_limit": None,
            "upper_limit": None,
        }
    ]

    assert response.json() == wanted_response


@pytest.mark.django_db()
@pytest.mark.usefixtures("_full_sensor_suite")
def test_complete_log(sample_logger):
    assert SensorData.objects.count() == 0

    json_data = """[{"name":"PAR1","value":1.03,"age":16,"idx":31},
    {"name":"Light1","value":NaN,"age":16,"idx":31},
    {"name":"PAR2","value":NaN,"age":16,"idx":31},
    {"name":"Light2","value":34.17,"age":16,"idx":31},
    {"name":"Temp1","value":26.49,"age":16,"idx":31},
    {"name":"Humidity","value":36.26,"age":16,"idx":31},
    {"name":"Temp2","value":26.12,"age":16,"idx":31},
    {"name":"Temp3","value":26.25,"age":15,"idx":31},
    {"name":"Usnd","value":72.70,"age":15,"idx":31},
    {"name":"EC","value":1.02,"age":14,"idx":31},
    {"name":"pH","value":5.40,"age":14,"idx":31},
    {"name":"CO2","value":659.00,"age":14,"idx":31}]"""
    data = json.loads(json_data)
    c = Client()
    url = f"/loggers/logger/{sample_logger.id}/"
    response = c.get(url)
    assert response.status_code == HTTP_200_OK
    url = f"/loggers/logger/{sample_logger.id}/log/"
    response = c.post(url, data, content_type="application/json")
    assert response.status_code == HTTP_201_CREATED
    expected_sensors_count = len([rec for rec in data if isfinite(rec["value"])])
    assert SensorData.objects.count() == expected_sensors_count


SB_REGISTER_DATA = json.dumps(
    {
        "name": "SB FC:F5:C4:55:85:20",
        "mac": "FC:F5:C4:55:85:20",
        "config": {
            "board_name": "SB FC:F5:C4:55:85:20",
            "enable_wifi": True,
            "ssid": "ssid",
            "password": "password",
            "use_dhcp": True,
            "box_ipaddr": "",
            "box_network": "",
            "box_gateway": "",
            "box_dns1": "",
            "box_dns2": "",
            "logger_host": "8.8.8.8",
            "logger_url": "",
        },
    }
)


@pytest.mark.django_db()
def test_logger_register(sample_logger):  # noqa: ARG001
    c = Client()
    url = "/loggers/logger/"
    response = c.post(url, SB_REGISTER_DATA, content_type="application/json")
    assert response.status_code == HTTP_201_CREATED
    response_data = json.loads(response.content.decode())
    assert response_data["name"] == "SB FC:F5:C4:55:85:20"
    assert "config" in response_data


@pytest.mark.django_db()
def test_logger_register_again(sample_logger):  # noqa: ARG001
    c = Client()
    url = "/loggers/logger/"
    response1 = c.post(url, SB_REGISTER_DATA, content_type="application/json")
    assert response1.status_code == HTTP_201_CREATED
    response = c.post(url, SB_REGISTER_DATA, content_type="application/json")
    assert response.status_code == HTTP_200_OK

    response_data = json.loads(response.content.decode())
    wanted_response = {
        "config": {
            "board_name": "SB FC:F5:C4:55:85:20",
            "box_dns1": "",
            "box_dns2": "",
            "box_gateway": "",
            "box_ipaddr": "",
            "box_network": "",
            "enable_wifi": True,
            "logger_host": "8.8.8.8",
            "logger_url": "",
            "password": "password",
            "ssid": "ssid",
            "use_dhcp": True,
        },
        "firmware": None,
        "id": ANY,
        "mac": "FC:F5:C4:55:85:20",
        "name": "SB FC:F5:C4:55:85:20",
        "sensors": "http://testserver/loggers/logger/24/sensor/",
    }
    assert wanted_response == response_data

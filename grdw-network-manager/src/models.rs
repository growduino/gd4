use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd, Eq, Ord)]
#[serde(rename_all = "camelCase")]
pub struct Wifi {
    pub ssid: String,
    pub strength: u32,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NetInterface {
    pub name: String,
    pub is_active: bool,
}

impl NetInterface {
    pub fn from_device(device: &network_manager::Device) -> Self {
        let state = device.get_state().unwrap_or(network_manager::DeviceState::Unknown);
        NetInterface {
            name: device.interface().to_string(),
            is_active: state == network_manager::DeviceState::Activated
        }
    }
}
from time import sleep
from typing import Any

import paho.mqtt.client as mqtt


def on_connect(client, *args, **kwargs):
    print("Connected")
    client.subscribe("logger/#")
    print("Subscribed")


def on_message(
    client: mqtt.Client,
    userdata: Any,
    msg: mqtt.MQTTMessage,
) -> None:
    print(msg.topic, msg.payload)


client = mqtt.Client(
    client_id="observer",
    callback_api_version=mqtt.CallbackAPIVersion.VERSION2,  # type: ignore[attr-defined]
    clean_session=True,
)
client.on_connect = on_connect
client.on_message = on_message
while not client.is_connected():
    print("Waiting for connection")
    client.connect(
        host="localhost",
        port=1883,
        keepalive=60,
    )
    print("Connected, starting loop")
    client.loop_forever()
    print("Forever passed")
    sleep(1)

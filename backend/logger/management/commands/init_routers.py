from typing import Any

from django.core.management.base import BaseCommand

from logger.models import ApparentSensor, Logger, Router, Sensor


class Command(BaseCommand):
    help = "Create default list of routers, if they do not exist. Use --reset to delete all routers first."
    requires_migrations_checks = True

    def handle(self, *_args: Any, **options: Any) -> None:
        if "reset" in options and options["reset"]:
            print("Deleting data.")  # noqa: T201
            Router.objects.all().delete()

        if Router.objects.all().count() > 0:
            return

        try:
            logger = Logger.objects.get(name="internal")
        except Logger.DoesNotExist:
            print("Logger 'internal' not found, not creating default routers")  # noqa: T201
            return
        for apparent_sensor in ApparentSensor.objects.all():
            print(f"Creating router for {apparent_sensor.name}")  # noqa: T201

            try:
                sensor = Sensor.objects.get(name=apparent_sensor.name, logger=logger)
            except Sensor.DoesNotExist:
                continue
            r = Router(apparent_sensor=apparent_sensor, aggregate="max", name=apparent_sensor.name)
            r.save()
            r.sensors.add(sensor)

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete all apparent sensors and create new. This erases history logs.",
        )

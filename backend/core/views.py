import datetime
import json
from math import isinf, isnan
import threading
from time import sleep
from typing import Any

from django.conf import settings
from django.core import mail
from django.core.mail.backends.smtp import EmailBackend
from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from core.forms import HtChangePass
from logger.models import (
    ApparentSensor,
    ApparentSensorData,
    ApparentSensorDataDaily,
    ApparentSensorDataHourly,
    CalibrationData,
    Logger,
    RawDataStore,
    Sensor,
    SensorData,
)
from logger.mqtt import mqtt_client

from .models import (
    Alert,
    JsonStorage,
    OutputLog,
    OutputRec,
    Trigger,
    get_config_field,
    new_config,
    save_config,
)
from .utils import (
    crop_seconds,
    eth_setup,
    get_daymin,
    jsonize,
    rename_htuser,
    set_htpass,
    tzinfo,
    wifi_clear,
    wifi_start,
)

lock = threading.Lock()


def get_sensor_data(_request: HttpRequest, name: str) -> JsonResponse:
    sensor = Sensor.objects.get(id=name)
    time_to = crop_seconds()
    time_from = time_to - datetime.timedelta(hours=1)

    records = SensorData.objects.select_related("sensor").filter(time__gt=time_from).filter(sensor=sensor)

    if records.count() == 0:
        raise Http404

    data = {}
    for record in records:
        if record is not None:
            when = record.time - time_from
            data[int(when.seconds / 60) - 1] = record.value
    result = jsonize(data, 60)

    resp = {"name": sensor.name, "min": result}

    return JsonResponse(resp)


def get_apparent_sensor_data(_request: HttpRequest, name: str) -> JsonResponse:
    sensor = ApparentSensor.objects.get(name__iexact=name)
    time_to = crop_seconds()
    time_from = time_to - datetime.timedelta(hours=1)

    records = ApparentSensorData.objects.select_related("sensor").filter(time__gt=time_from).filter(sensor=sensor)

    if records.count() == 0:
        raise Http404

    data = {}
    for record in records:
        if record is not None:
            when = record.time - time_from
            data[int(when.seconds / 60) - 1] = record.value
    result = jsonize(data, 60)

    resp = {"name": sensor.name, "min": result}

    return JsonResponse(resp)


def get_hourly_data(
    _request: HttpRequest,
    name: str,
    year: int,
    month: int,
    day: int,
    hour: int,
) -> JsonResponse:
    sensor = ApparentSensor.objects.get(name__iexact=name)

    time_from = datetime.datetime(
        year=int(year),
        month=int(month),
        day=int(day),
        hour=int(hour),
        tzinfo=tzinfo(),
    )
    time_to = time_from + datetime.timedelta(hours=1)

    records = ApparentSensorData.objects.select_related("sensor").filter(
        sensor=sensor,
        time__gte=time_from,
        time__lt=time_to,
    )
    if records.count() == 0:
        raise Http404

    data = {}
    for record in records:
        if record is not None:
            data[int(record.time.minute)] = record.value
    toret = jsonize(data, 60)
    return JsonResponse({"name": sensor.name, "min": toret})


def get_hourly_average(
    _request: HttpRequest,
    name: str,
    year: int,
    month: int,
    day: int,
) -> JsonResponse:
    sensor = ApparentSensor.objects.get(name__iexact=name)

    records = ApparentSensorDataHourly.objects.select_related("sensor").filter(
        sensor=sensor,
        time__year=year,
        time__month=month,
        time__day=day,
    )
    if records.count() == 0:
        raise Http404

    data = {}
    for record in records:
        if record is not None:
            record.time = timezone.localtime(record.time)
            data[int(record.time.hour)] = record.value
    result = jsonize(data, 24)

    return JsonResponse({"name": sensor.name, "h": result})


def get_daily_average(
    _request: HttpRequest,
    name: str,
    year: int,
    month: int,
) -> JsonResponse:
    sensor = ApparentSensor.objects.get(name__iexact=name)

    records = ApparentSensorDataDaily.objects.select_related("sensor").filter(
        sensor=sensor,
        time__year=year,
        time__month=month,
    )
    if records.count() == 0:
        raise Http404

    data = {}
    for record in records:
        if record is not None:
            record.time = timezone.localtime(record.time)
            data[int(record.time.day)] = record.value
    result = jsonize(data, 31, start=1)

    return JsonResponse({"name": sensor.name, "day": result})


@csrf_exempt
def get_trigger(request: HttpRequest, index: int) -> JsonResponse:
    """shows trigger as json
    {
        "t_since":-1,
        "t_until":0,
        "on_value":"<250",
        "off_value":">300",
        "sensor":1,
        "output":7,
        "active":1
    }"""
    if request.method == "POST" and not request.body:
        trigger = get_object_or_404(Trigger, index=index)
        trigger.delete()
        return JsonResponse({"result": "Deleted"})
    if request.body:
        return set_trigger(request, index)
    trigger = get_object_or_404(Trigger, index=index)

    return JsonResponse(trigger.as_dict())


# @logged_in_or_basicauth("Config")
def set_trigger(request: HttpRequest, index: int) -> JsonResponse:
    """creates or updates trigger
    not to be called directly"""
    # print(request.body.decode("utf-8"))
    jdata = json.loads(request.body.decode("utf-8"))
    # print(jdata)
    sensor = ApparentSensor.objects.get(index=jdata["sensor"])

    output = OutputRec.objects.get(index=jdata["output"])

    trigger, created = Trigger.objects.update_or_create(
        index=index,
        defaults={
            "t_since": jdata["t_since"],
            "t_until": jdata["t_until"],
            "on_value": jdata["on_value"],
            "off_value": jdata["off_value"],
            "active": jdata["active"],
            "sensor": sensor,
            "output": output,
        },
    )
    if created:
        resp = "Created"
    else:
        resp = "Updated"
    return JsonResponse({"result": resp})


@csrf_exempt
def get_alert(request: HttpRequest, index: int) -> JsonResponse:
    """shows alert as json
    {
        "on_message":"Temp too high!",
        "off_message":"Temp back at normal",
        "trigger":1,
        "target":"+420777123456",
        active": 1
    }"""
    if request.method == "POST" and not request.body:
        alert = get_object_or_404(Alert, index=index)
        alert.trigger.active = 0
        alert.trigger.save()
        alert.delete()
        return JsonResponse({"result": "Deleted"})
    if request.body:
        return set_alert(request, index)
    alert = get_object_or_404(Alert, index=index)
    jsontg: dict[str, Any] = {}
    jsontg["on_message"] = alert.on_message
    jsontg["off_message"] = alert.off_message
    jsontg["trigger"] = alert.trigger.index
    jsontg["target"] = alert.target
    jsontg["active"] = alert.active

    return JsonResponse(jsontg)


def get_output_states(time: datetime.datetime | None = None) -> str:
    outputs = OutputRec.objects.filter(index__gte=0)
    result = ""
    for state in [o.get_state(time) for o in outputs]:
        if state:
            result += "1"
        else:
            result += "0"
    return result


def outputs(
    _request: HttpRequest,
    year: int | None = None,
    month: int | None = None,
    day: int | None = None,
    sequence: int | None = None,
) -> JsonResponse:
    if year is None:  # posledni hodina
        start_time = timezone.now() - datetime.timedelta(hours=1)
        changes = OutputLog.objects.select_related("output").filter(output__index__gte=0).filter(time__gte=start_time)
    else:
        if str(sequence) != "0":
            raise Http404()
        changes = (
            OutputLog.objects.select_related("output")
            .filter(output__index__gte=0)
            .filter(time__year=year, time__month=month, time__day=day)
        )

    change_times = list({o.time for o in changes})
    change_times.sort()

    states = {}
    for change_time in change_times:
        states[str(int(change_time.timestamp()))] = str(int(get_output_states(change_time)[::-1], 2))

    result = {"name": "outputs", "state": states}

    return JsonResponse(result)


def output_changes(
    _request: HttpRequest,
    year: int | None = None,
    month: int | None = None,
    day: int | None = None,
    sequence: int | None = None,
) -> JsonResponse:
    if year is None or month is None or day is None:  # posledni hodina
        end_time = datetime.datetime.now(tzinfo())
        start_time = end_time - datetime.timedelta(hours=1)
        changes = OutputLog.objects.select_related("output").filter(output__index__gte=0).filter(time__gte=start_time)
    else:
        if str(sequence) != "0":
            raise Http404()
        changes = OutputLog.objects.filter(output__index__gte=0).filter(time__year=year, time__month=month, time__day=day)
        if changes:
            end_time = changes.latest().time
        else:
            end_time = timezone.datetime(  # type: ignore[attr-defined]
                int(year), int(month), int(day), tzinfo=timezone.now().tzinfo
            )

    states = {}

    for change in changes:
        change_time = str(int(change.time.timestamp()))
        if change_time not in states:
            states[change_time] = [{"output": change.output.name, "state": change.state}]
        else:
            states[change_time].append({"output": change.output.name, "state": change.state})

    end_state = get_output_states(end_time)

    result = {
        "name": "output_changes",
        "state": states,
        "end_state": str(int(end_state[::-1], 2)),
        "end_state_verbose": end_state,
    }

    return JsonResponse(result)


@csrf_exempt
def output_states(_request: HttpRequest) -> JsonResponse:
    result = get_output_states()
    jsontg = {"outputs": result}
    return JsonResponse(jsontg)


def set_alert(request: HttpRequest, index: int) -> JsonResponse:
    """creates or updates alert
    not to be called directly"""
    # print(request.body.decode("utf-8"))
    jdata = json.loads(request.body.decode("utf-8"))
    trigger, t_created = Trigger.objects.get_or_create(index=jdata["trigger"])

    alert, created = Alert.objects.update_or_create(
        index=index,
        defaults={
            "on_message": jdata["on_message"],
            "off_message": jdata["off_message"],
            "trigger": trigger,
            "target": jdata["target"],
            "active": jdata["active"],
        },
    )
    alert.trigger.active = 1
    alert.trigger.save()
    if created:
        resp = "Created"
    else:
        resp = "Updated"
    return JsonResponse({"result": resp})


@csrf_exempt
# @logged_in_or_basicauth("Logging")
def sensor_log(
    request: HttpRequest, name: str | None = None, value: float | None = None, logger: int | None = None
) -> JsonResponse:
    from logger.models import Sensor

    if name is not None:
        if logger is None:
            logger_ = Logger.objects.get(name="internal")
        else:
            logger_ = get_object_or_404(Logger, id=logger)
        sensor = get_object_or_404(Sensor, name=name, logger=logger_)
        if sensor.log(value) is not None:
            json_response = {"result": "ok"}
            result = 200
        else:
            json_response = {"result": "Invalid value"}
            result = 400
    elif request.method == "POST":
        data = json.loads(request.body.decode("utf-8"))
        if data:
            if "rawvalue" in data:
                sensor = Sensor.objects.get(name__iexact=data["sensor"], logger__name="internal")
                raw_data, created = RawDataStore.objects.get_or_create(sensor=sensor)
                raw_data.value = data["rawvalue"]
                raw_data.measured = True
                raw_data.save()
            else:
                for record in data:
                    sensor = Sensor.objects.get(name=record["name"], logger__name="internal")
                    sensor.log(record["value"])
            json_response = {"result": "ok"}
            result = 200
        else:
            json_response = {"result": "noop"}
            result = 200
    else:
        json_response = {"result": "Invalid data"}
        result = 400

    return JsonResponse(json_response, status=result)


def status(_request: HttpRequest) -> JsonResponse:
    """
    {
        "free_ram":1025,
        "sensors":6,
        "outputs":8,
        "sensor_list":{
            "0":"Humidity",
            "1":"Temp1",
            "2":"Light",
            "3":"Usnd",
            "4":"Temp2",
            "5":"Temp3"
        },
        "triggers":10,
        "triggers_log_size":25,
        "uptime":"3381",
        "daymin":234
    }"""
    sensors = ApparentSensor.objects.filter(index__gte=0)
    sensor_list = {}
    for s in sensors:
        sensor_list[str(s.index)] = s.name

    toret = {
        "sensors": sensors.count(),
        "outputs": 12,
        "triggers": 100,
        "daymin": get_daymin(),
        "sensor_list": sensor_list,
        "time_zone": getattr(settings, "TIME_ZONE", "Error"),
    }
    return JsonResponse(toret)


@csrf_exempt
# @logged_in_or_basicauth("Config")
def json_store(request: HttpRequest, filename: str = "config") -> JsonResponse:
    if request.method == "POST":
        if not request.body:
            config = new_config(filename)
            save_config(config, filename)
            announce_config_change(filename)
            return JsonResponse({"result": "reset to default"})

        save_config(json.loads(request.body.decode("utf-8")), filename=filename)
        announce_config_change(filename)
    return JsonResponse(get_config_field(filename=filename))


def announce_config_change(filename: str) -> None:
    if filename == "config":
        config = get_config_field()

        if "wifi_ssid" in config and config["wifi_ssid"]:
            ssid = config["wifi_ssid"]
            password = None
            if "wifi_pwd" in config and config["wifi_ssid"]:
                password = str(config["wifi_pwd"])

            wifi_start(ssid, password)
        else:
            wifi_clear()
        sleep(10)
        eth_setup()

    mqtt_client.publish("logger/internal", filename)


@csrf_exempt
def json_store_partial(request: HttpRequest, filename: str) -> JsonResponse:
    if request.method == "POST":
        if not request.body:
            return JsonResponse({"result": "failed"}, status=400)
        request_config = json.loads(request.body.decode("utf-8"))
        db_config = get_config_field(filename=filename)

        for field in request_config:
            if request_config[field] in (None, ""):
                db_config.pop(field, None)
            else:
                db_config[field] = request_config[field]
        config_db = JsonStorage(value=json.dumps(db_config), filename=filename)
        config_db.save()

        announce_config_change(filename)
        return JsonResponse({"result": "ok"})
    return JsonResponse({"result": "failed"}, status=400)


@csrf_exempt
def sensor_calibrate(_request: HttpRequest) -> JsonResponse:
    jsontg = {"result": "ok"}
    return JsonResponse(jsontg)


def wifi_list(_request: HttpRequest) -> JsonResponse:
    """
    wifis = {}
    for dev in NetworkManager.NetworkManager.GetDevices():
        if dev.DeviceType != NetworkManager.NM_DEVICE_TYPE_WIFI:
            continue
        for ap in dev.GetAccessPoints():
            security = flags_to_security(ap.Flags, ap.WpaFlags, ap.RsnFlags)
            wifi = {
                "ssid": ap.Ssid,
                "frequency": ap.Frequency,
                "encrypted": security != "",
                "security": security,
                "quality": f"{ap.Strength}%",
                "channel": channelate(ap.Frequency),
            }
            strength = ap.Strength
            while strength in wifis:
                strength += 1
            wifis[strength] = wifi
    sorted_wifi = []
    for key in sorted(wifis.keys()):
        sorted_wifi.append(wifis[key])
    sorted_wifi.reverse()
    """
    sorted_wifi: dict[str, Any] = {}
    return JsonResponse({"networks": sorted_wifi})


def wifi_active(_request: HttpRequest) -> JsonResponse:
    """
    ssid = None
    for conn in NetworkManager.NetworkManager.ActiveConnections:
        settings = conn.Connection.GetSettings()
        if "802-11-wireless" in settings:
            ssid = settings["802-11-wireless"]["ssid"]
    """
    ssid = "???"
    return JsonResponse({"ssid": ssid})


def calibrate(_request: HttpRequest, index: int) -> JsonResponse:
    sensor = get_object_or_404(Sensor, index=index)
    sensor.raw_data.all().delete()

    line_to_send = f"get raw {sensor.name.lower()}"
    mqtt_client.publish("logger/internal", line_to_send)

    start_time = datetime.datetime.now()
    duration = datetime.timedelta(seconds=20)
    stop_time = start_time + duration

    while sensor.raw_data.filter(measured=True).count() == 0:
        if datetime.datetime.now() > stop_time:
            return JsonResponse({"result": "internal timeout"}, status=504)
        sleep(1)

    raw_data: RawDataStore = sensor.raw_data.filter(measured=True)[0]
    raw_value = raw_data.value
    if raw_value is None or isinf(raw_value) or isnan(raw_value):
        raw_value = -999
    sensor.raw_data.all().delete()
    return JsonResponse({"raw_value": raw_value})


def password_change(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = HtChangePass(request.POST)
        if form.is_valid():
            done = False
            username = form.cleaned_data["username"]
            new_password = form.cleaned_data["new_password"]
            old_password = form.cleaned_data["old_password"]
            rename_to = form.cleaned_data["rename_to"]
            if rename_to:
                rename_htuser(username, rename_to, old_password)
                username = rename_to
                done = True
            if new_password:
                set_htpass(username, new_password, old_password)
                done = True
            if done:
                return HttpResponseRedirect("/")
    else:
        form = HtChangePass()

    return render(request, "core/password_change.html", {"form": form})


def send_test_mail(request: HttpRequest) -> JsonResponse:
    if "to" in request.GET and "@" in request.GET["to"]:
        target = (str(request.GET["to"]),)
        host = request.GET.get("smtp")
        port = request.GET.get("smtp_port")
        username = request.GET.get("smtp_user")
        password = request.GET.get("smtp_pwd")
        smtp_ssl = request.GET.get("smtp_ssl")
        use_tls = smtp_ssl == "tls"
        use_ssl = smtp_ssl == "ssl"
        mail_from = request.GET.get("mail_from")

    else:
        return JsonResponse({"success": False, "code": "{resp}".format(resp="missing sender")})

    subject = "[grdw] test mail"
    body = "Hello, this is just testing mail."

    debug_smtp = {
        "host": host,
        "port": port,
        "username": username,
        "password": password,
        "use_ssl": use_ssl,
        "use_tls": use_tls,
        "from_email": mail_from,
        "to": target,
    }

    try:
        backend = EmailBackend(
            host=host,
            port=port,
            username=username,
            password=password,
            use_ssl=use_ssl,
            use_tls=use_tls,
            timeout=5,
        )

        mail.EmailMessage(
            subject=subject,
            body=body,
            from_email=mail_from,
            to=target,
            connection=backend,
        ).send()
    except Exception as err:
        return JsonResponse({"success": False, "code": f"{err}", "settings": debug_smtp})

    return JsonResponse({"success": True, "settings": debug_smtp})


@csrf_exempt
def calib_new(request: HttpRequest, sensor: int) -> JsonResponse:
    calib = CalibrationData()
    sensor_ = get_object_or_404(Sensor, id=sensor)

    if request.method == "POST":
        calib.sensor = sensor_
        calib.sensor_reading = request.POST["sensor_reading"]
        calib.real_value = request.POST["real_value"]
        calib.save()
        sensor_.update_calibration()
        return JsonResponse({"success": True, "data": calib.as_dict()})
    return JsonResponse({"success": False})


def calib_list(_request: HttpRequest, sensor: int) -> JsonResponse:
    sensor_: Sensor = get_object_or_404(Sensor, index=sensor)
    json = {calib.id: calib.as_dict() for calib in sensor_.calibration_data.all()}
    return JsonResponse(json)


def calib_get(_request: HttpRequest, sensor: int, object_id: int) -> JsonResponse:
    calib = get_object_or_404(CalibrationData, id=object_id, sensor=Sensor.objects.get(index=sensor))
    return JsonResponse(calib.as_dict())


def calib_delete(_request: HttpRequest, sensor: int, object_id: int) -> JsonResponse:
    calib = get_object_or_404(CalibrationData, id=object_id, sensor=Sensor.objects.get(index=sensor))
    calib.delete()
    sensor_ = Sensor.objects.get(index=sensor)
    sensor_.update_calibration()
    return JsonResponse({"success": True})


@csrf_exempt
def calib_set(request: HttpRequest, sensor: int, object_id: int) -> JsonResponse:
    sensor_rec = get_object_or_404(Sensor, index=sensor)
    calib = get_object_or_404(CalibrationData, id=object_id, sensor=sensor_rec)
    if request.method == "POST":
        calib.sensor_reading = request.POST["sensor_reading"]
        calib.real_value = request.POST["real_value"]
        calib.save()
        sensor_rec.update_calibration()
        return JsonResponse({"success": True, "data": calib.as_dict()})
    return JsonResponse({"success": False})

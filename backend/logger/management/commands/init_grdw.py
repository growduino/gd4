from typing import Any

from django.core.management.base import BaseCommand, CommandParser

from logger.management.commands import (
    init_appparent_sensors,
    init_internal_sensors,
    init_lcd_lines,
    init_outputs,
    init_routers,
    reset_admin_password,
)
from logger.models import Logger


class Command(BaseCommand):
    help = "Run all growduino init commands"

    def handle(self, *args: Any, **options: Any) -> None:
        print("Running init commands...")  # noqa: T201
        Logger.objects.filter(name="internal").exclude(id=1).delete()
        if Logger.objects.filter(id=1).count() == 0:
            options["reset"] = True

        init_appparent_sensors.Command().handle(*args, **options)
        init_internal_sensors.Command().handle(*args, **options)
        init_routers.Command().handle(*args, **options)
        init_outputs.Command().handle(*args, **options)
        init_lcd_lines.Command().handle(*args, **options)
        reset_admin_password.Command().handle(*args, **options)

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete current objects and create new ones.",
        )

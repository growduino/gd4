import logging
from time import sleep
import uuid

from django.conf import settings
import paho.mqtt.client as mqtt

log = logging.getLogger(__name__)

MAX_PUBLISH_TRIES = 5


def on_connect(client, userdata, flags, rc, props):
    if rc == 0:
        log.info("Connected to MQTT Broker")
    else:
        log.error("Connection failed with result code: " + str(rc))


def on_disconnect(client, userdata, rc, props):
    log.info("Disconnected from MQTT Broker")


def on_publish(client, *args):
    log.debug("Published to MQTT Broker: " + str(args))


class MqttClient:
    _client: mqtt.Client

    def __init__(self) -> None:
        self._client = mqtt.Client(
            client_id="django-" + uuid.uuid4().hex,
            callback_api_version=mqtt.CallbackAPIVersion.VERSION2,  # type: ignore[attr-defined]
            clean_session=True,
        )
        self._client.on_connect = on_connect
        self._client.on_publish = on_publish

    def connect(self) -> None:
        try:
            self._client.connect(
                host=settings.RMQTT_HOST,
                port=1883,
                keepalive=60,
            )
            self._client.loop_start()
        except OSError:
            log.error("Cannot connect to MQTT broker")

    def publish(self, topic: str, payload: str) -> None:
        log.debug(f"sending MQTT message to {topic}: {payload}")
        count = 0

        while count < MAX_PUBLISH_TRIES:
            while not self._client.is_connected():
                self.connect()
                sleep(0.1)
            res = self._client.publish(topic, payload)
            try:
                res.wait_for_publish(2)
            except (ValueError, RuntimeError) as e:
                log.error(e)
                sleep(1)
                continue
            if res[0] == mqtt.MQTT_ERR_SUCCESS:
                log.debug(
                    "Published message with result %s, published? %s",
                    res,
                    res.is_published(),
                )
                return
            log.warning("Publish failed with result %s", res)
            count += 1
            sleep(1)


mqtt_client = MqttClient()

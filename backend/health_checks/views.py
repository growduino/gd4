# Create your views here.
from django.http import JsonResponse

from logger.models import Logger


def live(_request):
    return JsonResponse({"status": "ok"})


def ready(_request):
    count = Logger.objects.all().count()
    if count > 0:
        return JsonResponse({"status": "ok"})

    return JsonResponse({"status": "not ready yet"}, status=503)

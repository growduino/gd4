"""growduino_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.urls import re_path
from django.views import defaults
from django.views.generic import RedirectView

from . import views
from .trigger import generate_averages, run_cron

app_name = "core"

urlpatterns = [
    re_path(r"^$", RedirectView.as_view(url="/index.htm", permanent=False), name="home"),
    re_path(r"^alerts/(?P<index>[0-9]+).jso$", views.get_alert),
    re_path(r"^triggers/(?P<index>[0-9]+).jso$", views.get_trigger),
    re_path(r"^sensors/status.jso$", views.status),
    # /sensors/{tempX,humidity,light,...}.jso
    re_path(r"^sensors/outputs.jso$", views.outputs),
    re_path(r"^sensors/output_changes.jso$", views.output_changes),
    re_path(r"^output_states.jso$", views.output_states),
    re_path(r"^sensors/(?P<name>[0-9]+).jso$", views.get_sensor_data),
    re_path(r"^sensors/(?P<name>[a-zA-Z][0-9a-zA-Z]+).jso$", views.get_apparent_sensor_data),
    re_path(r"^sensors/rawdata/(?P<index>[0-9]+).jso$", views.calibrate),
    re_path(
        r"^(data|DATA)/(outputs|OUTPUTS)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<sequence>[0-9]+).(jso|JSO)$",
        views.outputs,
    ),
    re_path(
        r"^(data|DATA)/(output_changes|OUTPUT_CHANGES)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<sequence>[0-9]+).jso$",
        views.output_changes,
    ),
    re_path(
        r"^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+)/(?P<hour>[0-9]+).(jso|JSO)$",
        views.get_hourly_data,
    ),
    re_path(r"^logger/(?P<name>[0-9a-zA-Z]+)/(?P<value>[0-9.]*)$", views.sensor_log),
    re_path(r"^logger/(?P<logger>[0-9]+)/(?P<name>[0-9a-zA-Z]+)/(?P<value>[0-9.]*)$", views.sensor_log),
    re_path(r"^log$", views.sensor_log),
    re_path(r"^(?P<filename>config).jso$", views.json_store),
    re_path(r"^(?P<filename>client).jso$", views.json_store),
    re_path(r"^(?P<filename>calib).jso$", views.json_store),
    re_path(r"^(?P<filename>fanconfig).jso$", views.json_store),
    re_path(r"^wifilist.jso$", views.wifi_list),
    re_path(r"^wifi_active.jso$", views.wifi_active),
    re_path(r"^partial/(?P<filename>config).jso$", views.json_store_partial),
    re_path(r"^partial/(?P<filename>client).jso$", views.json_store_partial),
    re_path(r"^partial/(?P<filename>calib).jso$", views.json_store_partial),
    re_path(r"^partial/(?P<filename>fanconfig).jso$", views.json_store_partial),
    re_path(
        r"^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+).(jso|JSO)$",
        views.get_daily_average,
    ),
    re_path(
        r"^(data|DATA)/(?P<name>[0-9a-zA-Z]+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+).(jso|JSO)$",
        views.get_hourly_average,
    ),
    re_path(r"^change_password", views.password_change),
    re_path("^webcam", defaults.page_not_found),
    re_path("^send_test_mail", views.send_test_mail),
    re_path(r"^calibrate/(?P<sensor>[0-9a-zA-Z]+)/new$", views.calib_new),
    re_path(r"^calibrate/(?P<sensor>[0-9a-zA-Z]+)/list.jso$", views.calib_list),
    re_path(r"^calibrate/(?P<sensor>[0-9a-zA-Z]+)/get/(?P<id>[0-9]+).jso$", views.calib_get),
    re_path(r"^calibrate/(?P<sensor>[0-9a-zA-Z]+)/set/(?P<id>[0-9]+).jso$", views.calib_set),
    re_path(
        r"^calibrate/(?P<sensor>[0-9a-zA-Z]+)/delete/(?P<id>[0-9]+).jso$",
        views.calib_delete,
    ),
    re_path(r"^run_cron", run_cron),
    re_path(r"^generate_averages", generate_averages),
]

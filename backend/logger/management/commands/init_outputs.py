from typing import Any

from django.core.management.base import BaseCommand, CommandParser

from core.models import OutputRec

OUTPUTS = [(f"Output #{idx}", idx) for idx in range(10)]


class Command(BaseCommand):
    help = "Create default list of outputs, if they do not exist. Use --reset to delete all and create new."
    requires_migrations_checks = True

    def handle(self, *_args: Any, **options: Any) -> None:
        if "reset" in options and options["reset"]:
            print("Deleting data.")  # noqa: T201
            OutputRec.objects.all().delete()

        if OutputRec.objects.count() == 0:
            print("Creating outputs...")  # noqa: T201
            for name, index in OUTPUTS:
                print(f"Creating {name}")  # noqa: T201
                output = OutputRec(name=name, index=index)
                output.save()

    def add_arguments(self, parser: CommandParser):
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete all outputs and create new. This erases history logs.",
        )

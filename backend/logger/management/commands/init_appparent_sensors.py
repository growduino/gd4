from typing import Any

from django.core.management.base import BaseCommand

from logger.const import APPARENT_SENSORS
from logger.models import ApparentSensor


class Command(BaseCommand):
    help = "Create default list of apparent sensors, if they do not exist. Use --reset to delete all and create new."
    requires_migrations_checks = True

    def handle(self, *_args: Any, **options: Any) -> None:
        if "reset" in options and options["reset"]:
            print("Deleting data.")  # noqa: T201
            ApparentSensor.objects.all().delete()

        if ApparentSensor.objects.count() == 0:
            print("Creating virtual sensors...")  # noqa: T201
            for name, sensor_type, index in APPARENT_SENSORS:
                print(f"Creating virtual sensor {name}")  # noqa: T201
                sensor = ApparentSensor(name=name, sensor_type=sensor_type, index=index)
                sensor.save()

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            action="store_true",
            help="Delete all apparent sensors and create new. This erases history logs.",
        )

from logger.const import INTERNAL_SENSORS
from logger.models import Logger, Sensor


def create_sensors(logger: Logger) -> None:
    for name, type_, lower, upper, index in INTERNAL_SENSORS:
        if index is None:
            sensor, created = Sensor.objects.get_or_create(
                name=name,
                logger=logger,
                defaults={
                    "index": index,
                    "sensor_type": type_,
                    "lower_limit": lower,
                    "upper_limit": upper,
                },
            )
        else:
            sensor, created = Sensor.objects.get_or_create(
                name=name,
                logger=logger,
                defaults={
                    "sensor_type": type_,
                    "lower_limit": lower,
                    "upper_limit": upper,
                },
            )
        if created:
            sensor.save()
